package com.gudanggaramtbk.server.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.gudanggaramtbk.server.dao.visitDao;
import com.gudanggaramtbk.server.model.Site;
import com.gudanggaramtbk.server.model.Visit;
import com.gudanggaramtbk.server.model.request.VisitAssignAccessCardRequest;
import com.gudanggaramtbk.server.model.request.VisitEndRequest;
import com.gudanggaramtbk.server.model.request.VisitSaveRequest;
import io.swagger.v3.oas.annotations.Operation;


@RestController
@RequestMapping(value = "/ggguestbook/visit/")
public class VisitController {
	@Resource 
	visitDao 	visitDb;

 
	@CrossOrigin(origins = "*")
	@Operation(summary = "get data visit by id")	
	@RequestMapping(value = "/getVisitbyId/{p_visitId}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<Visit> _getVisitbyId(@PathVariable Integer p_visitId){		
		Visit result;		
		try {	
			result = visitDb.getVisitbyId(p_visitId); 
			if(result != null) {
				try {					
					return new ResponseEntity<Visit>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getVisitbyId Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Visit>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Visit>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Visit>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	

	@CrossOrigin(origins = "*")
	@Operation(summary = "get visit list by guestId")	
	@RequestMapping(value = "/getVisitbyguestId/{p_guestId}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<List<Visit>> _getVisitbyguestId(@PathVariable Integer p_guestId){		
		List<Visit> result;		
		try {	
			result = visitDb.getVisitbyguestId(p_guestId);  
			if(result != null) {
				try {					
					return new ResponseEntity<List<Visit>>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getVisitbyguestId Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<List<Visit>>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<List<Visit>>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<List<Visit>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}		

	@CrossOrigin(origins = "*")
	@Operation(summary = "get visit expired list")	
	@RequestMapping(value = "/getVisitExpired/{p_siteCode}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<List<Visit>> _getVisitExpired(@PathVariable String p_siteCode){		
		List<Visit> result;		
		try {	
			result = visitDb.getVisitExpired(p_siteCode);  
			if(result != null) {
				try {					
					return new ResponseEntity<List<Visit>>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getVisitExpired Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<List<Visit>>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<List<Visit>>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<List<Visit>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}		
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "get data visit by session id")	
	@RequestMapping(value = "/getVisitbySessionCode/{p_visitSessionCode}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<Visit> _getVisitbySessionCode(@PathVariable String p_visitSessionCode){		
		Visit result;		
		try {	
			result = visitDb.getVisitbySessionCode(p_visitSessionCode); 
			if(result != null) {
				try {					
					return new ResponseEntity<Visit>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getVisitbySessionCode Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Visit>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Visit>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Visit>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	

	@CrossOrigin(origins = "*")
	@Operation(summary = "get data Active visit by guest card id")	
	@RequestMapping(value = "/getActiveVisitbyguestCardId/{p_guestCardId}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<Visit> _getActiveVisitbyguestCardId(@PathVariable String p_guestCardId){		
		Visit result;		
		Visit noresult; 
		noresult = new Visit(); 
		noresult.setVisitId(-1); 
		
		try {	
			result = visitDb.getActiveVisitbyguestCardId(p_guestCardId); 
			if(result != null) {
				try {					
					return new ResponseEntity<Visit>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getActiveVisitbyguestCardId Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Visit>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Visit>(noresult, HttpStatus.OK);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Visit>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	

	@CrossOrigin(origins = "*")
	@Operation(summary = "get visit list by site and date")	
	@RequestMapping(value = "/getVisitByDate/{p_siteCode}/{p_visitDate}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<List<Visit>> _getVisitByDate(@PathVariable String p_siteCode, @PathVariable String p_visitDate){		
		List<Visit> result;		
		try {	
			result = visitDb.getVisitByDate(p_siteCode, p_visitDate); 
			if(result != null) {
				try {					
					return new ResponseEntity<List<Visit>>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getVisitByDate Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<List<Visit>>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<List<Visit>>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<List<Visit>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}		
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "set Regis Visit")	
	@RequestMapping(value = "/setsaveVisit", method = RequestMethod.POST, produces = "application/json")		
	public ResponseEntity<Visit> _setSaveVisit(@RequestBody VisitSaveRequest p_visitReqObj){		
		Visit result;	
		try {				    	
			result = visitDb.setSaveVisit(p_visitReqObj);	
			if(result != null) {
				try {					
					return new ResponseEntity<Visit>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_setSaveVisit Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Visit>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Visit>(result, HttpStatus.NO_CONTENT);								
			}			
	   }
	   catch(Exception e) {		   
		   return new ResponseEntity<Visit>(HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "set End Visit")	
	@RequestMapping(value = "/setendVisit", method = RequestMethod.POST, produces = "application/json")		
	public ResponseEntity<Visit> _setendVisit(@RequestBody VisitEndRequest p_endVisitObj){		
		Visit result;	
		try {				    	
			result = visitDb.setendVisit(p_endVisitObj);	
			if(result != null) {
				try {					
					return new ResponseEntity<Visit>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_setendVisit Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Visit>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Visit>(result, HttpStatus.NO_CONTENT);								
			}			
	   }
	   catch(Exception e) {		   
		   return new ResponseEntity<Visit>(HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}	

	@CrossOrigin(origins = "*")
	@Operation(summary = "set Assign Access Card")	
	@RequestMapping(value = "/setAssignAccessCard", method = RequestMethod.POST, produces = "application/json")		
	public ResponseEntity<Visit> _setAssignAccessCard(@RequestBody VisitAssignAccessCardRequest p_VisitAssignCardObj){		
		Visit result;	
		try {				    	
			result = visitDb.setAssignAccessCard(p_VisitAssignCardObj);	
			if(result != null) {
				try {					
					return new ResponseEntity<Visit>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_setendVisit Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Visit>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Visit>(result, HttpStatus.NO_CONTENT);								
			}			
	   }
	   catch(Exception e) {		   
		   return new ResponseEntity<Visit>(HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}	
 
}
