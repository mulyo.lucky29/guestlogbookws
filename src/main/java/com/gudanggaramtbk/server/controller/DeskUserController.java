package com.gudanggaramtbk.server.controller;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.gudanggaramtbk.server.dao.deskUserDao;
import com.gudanggaramtbk.server.model.Contact;
import com.gudanggaramtbk.server.model.DeskUser;
import com.gudanggaramtbk.server.model.Visit;
import com.gudanggaramtbk.server.model.request.DeskUserLoginSaveRequest;
import com.gudanggaramtbk.server.model.request.DeskUserLoginRequest;
import com.gudanggaramtbk.server.model.request.DeskUserLoginResetRequest;
import com.gudanggaramtbk.server.model.request.VisitSaveRequest;
import io.swagger.v3.oas.annotations.Operation;


@RestController
@RequestMapping(value = "/ggguestbook/deskuser/")
public class DeskUserController {
	@Resource 
	deskUserDao 	deskUserDb;

	//  public List<DeskUser> getListUserBySite(String p_siteId) { 

	@CrossOrigin(origins = "*")
	@Operation(summary = "get user list by siteId")	
	@RequestMapping(value = "/login/list/{p_siteId}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<List<DeskUser>> _getListUserBySite(@PathVariable Integer p_siteId){		
		List<DeskUser> result;		
		try {	
			result = deskUserDb.getListUserBySite(p_siteId);  
			if(result != null) {
				try {					
					return new ResponseEntity<List<DeskUser>>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getListUserBySite Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<List<DeskUser>>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<List<DeskUser>>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<List<DeskUser>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}		

	@CrossOrigin(origins = "*")
	@Operation(summary = "get user list")	
	@RequestMapping(value = "/login/list/all", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<List<DeskUser>> _getListAllUser(){		
		List<DeskUser> result;		
		try {	
			result = deskUserDb.getListAllUser();  
			if(result != null) {
				try {					
					return new ResponseEntity<List<DeskUser>>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getListAllUser Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<List<DeskUser>>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<List<DeskUser>>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<List<DeskUser>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}		

	@CrossOrigin(origins = "*")
	@Operation(summary = "get profile desk user by id")	
	@RequestMapping(value = "/login/profile/{p_userId}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<DeskUser> _getUserById(@PathVariable Integer p_userId){		
		DeskUser result;		
		try {	
			result = deskUserDb.getUserById(p_userId); 
			if(result != null) {
				try {					
					return new ResponseEntity<DeskUser>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getUserById Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<DeskUser>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<DeskUser>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<DeskUser>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	

	@CrossOrigin(origins = "*")
	@Operation(summary = "do desk Login")	
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")		
	public ResponseEntity<DeskUser> _getLogin(@RequestBody DeskUserLoginRequest p_loginReqObj){		
		DeskUser result;	
		try {				    	
			result = deskUserDb.getLogin(p_loginReqObj);	
			if(result != null) {
				try {					
					return new ResponseEntity<DeskUser>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getLogin Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<DeskUser>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<DeskUser>(result, HttpStatus.NO_CONTENT);								
			}			
	   }
	   catch(Exception e) {		   
		   return new ResponseEntity<DeskUser>(HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}

	@CrossOrigin(origins = "*")
	@Operation(summary = "save desk Login")	
	@RequestMapping(value = "/login/save", method = RequestMethod.POST, produces = "application/json")		
	public ResponseEntity<DeskUser> _saveLogin(@RequestBody DeskUserLoginSaveRequest p_loginAccObj){		
		DeskUser result;	
		try {				    	
			result = deskUserDb.saveLogin(p_loginAccObj);	
			if(result != null) {
				try {					
					return new ResponseEntity<DeskUser>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getLogin Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<DeskUser>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<DeskUser>(result, HttpStatus.NO_CONTENT);								
			}			
	   }
	   catch(Exception e) {		   
		   return new ResponseEntity<DeskUser>(HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}

	@CrossOrigin(origins = "*")
	@Operation(summary = "Delete desk Login")	
	@RequestMapping(value = "/login/delete/{p_userId}", method = RequestMethod.DELETE, produces = "application/json")	
	public ResponseEntity<DeskUser> _deleteLogin(@PathVariable Integer p_userId){		
		DeskUser result;		
		try {	
			result = deskUserDb.deleteLogin(p_userId);
			if(result != null) {
				try {					
					return new ResponseEntity<DeskUser>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_deleteLogin Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<DeskUser>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<DeskUser>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<DeskUser>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "change password desk Login")	
	@RequestMapping(value = "/login/changePassword", method = RequestMethod.POST, produces = "application/json")		
	public ResponseEntity<DeskUser> _changePassword(@RequestBody DeskUserLoginResetRequest p_loginResetObj){		
		DeskUser result;	
		try {				    	
			result = deskUserDb.changePassword(p_loginResetObj); 	
			if(result != null) {
				try {					
					return new ResponseEntity<DeskUser>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_changePassword Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<DeskUser>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<DeskUser>(result, HttpStatus.NO_CONTENT);								
			}			
	   }
	   catch(Exception e) {		   
		   return new ResponseEntity<DeskUser>(HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}

	
}