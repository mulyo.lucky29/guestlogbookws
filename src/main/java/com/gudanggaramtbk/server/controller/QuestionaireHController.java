package com.gudanggaramtbk.server.controller;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.gudanggaramtbk.server.dao.questionaireHDao;
import com.gudanggaramtbk.server.model.QuestionaireH;
import com.gudanggaramtbk.server.model.request.QuestionaireHSaveRequest;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping(value = "/ggguestbook/questionaireH/")
public class QuestionaireHController {
	@Resource 
	questionaireHDao 	questionaireHDb;


	@CrossOrigin(origins = "*")
	@Operation(summary = "get form questionaire by id")	
	@RequestMapping(value = "/getQuestionaire/{p_queId}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<QuestionaireH> _getQuestionaireById(@PathVariable Integer p_queId){		
		QuestionaireH result;		
		
		try {	
			result = questionaireHDb.getQuestionaireById(p_queId); 
			if(result != null) {
				try {					
					return new ResponseEntity<QuestionaireH>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getQuestionaireById Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<QuestionaireH>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<QuestionaireH>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<QuestionaireH>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	

	@CrossOrigin(origins = "*")
	@Operation(summary = "get list form questionaire")	
	@RequestMapping(value = "/getListQuestionaire", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<List<QuestionaireH>> _getListQuestionaire(){		
		List<QuestionaireH> result;		
		try {	
			result = questionaireHDb.getListQuestionaire(); 
			if(result != null) {
				try {					
					return new ResponseEntity<List<QuestionaireH>>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getListLookupByGroup Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<List<QuestionaireH>>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<List<QuestionaireH>>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<List<QuestionaireH>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}		

	@CrossOrigin(origins = "*")
	@Operation(summary = "set save form questionaire")	
	@RequestMapping(value = "/saveForm", method = RequestMethod.POST, produces = "application/json")		
	public ResponseEntity<QuestionaireH> _saveQuehForm(@RequestBody QuestionaireHSaveRequest p_queHSave){		
		QuestionaireH result;	
		try {				    	
			result = questionaireHDb.saveQuehForm(p_queHSave);	
			if(result != null) {
				try {					
					return new ResponseEntity<QuestionaireH>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_saveQuehForm Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<QuestionaireH>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<QuestionaireH>(result, HttpStatus.NO_CONTENT);								
			}			
	   }
	   catch(Exception e) {		   
		   return new ResponseEntity<QuestionaireH>(HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}

	@CrossOrigin(origins = "*")
	@Operation(summary = "delete form questionaire")	
	@RequestMapping(value = "/delete/{p_quehId}", method = RequestMethod.DELETE, produces = "application/json")	
	public ResponseEntity<QuestionaireH> _deleteLogin(@PathVariable Integer p_quehId){		
		QuestionaireH result;		
		try {	
			result = questionaireHDb.deleteQuehForm(p_quehId);
			if(result != null) {
				try {					
					return new ResponseEntity<QuestionaireH>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_deleteLogin Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<QuestionaireH>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<QuestionaireH>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<QuestionaireH>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
}
