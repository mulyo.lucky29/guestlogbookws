package com.gudanggaramtbk.server.controller;

import java.util.List;

import javax.annotation.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.gudanggaramtbk.server.dao.checkLogDao;
import com.gudanggaramtbk.server.model.CheckLog;
import com.gudanggaramtbk.server.model.request.VisitCheckLogRequest;
import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping(value = "/ggguestbook/checklog/")
public class CheckLogController {
	@Resource 
	checkLogDao 	checkLogDb;
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "get data check log by id")	
	@RequestMapping(value = "/getCheckLogById/{p_check_id}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<CheckLog> _getCheckLogById(@PathVariable Integer p_check_id){		
		CheckLog result;		
		try {	
			result = checkLogDb.getCheckLogById(p_check_id); 
			if(result != null) {
				try {					
					return new ResponseEntity<CheckLog>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getSiteById Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<CheckLog>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<CheckLog>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<CheckLog>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}		
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "get List data check log by visitCode")	
	@RequestMapping(value = "/getListCheckLogByVisitCode/{p_visit_code}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<List<CheckLog>> _getListCheckLogByVisitCode(@PathVariable String p_visit_code){		
		List<CheckLog> result;		
		try {	
			result = checkLogDb.getListCheckLogByVisitCode(p_visit_code); 
			if(result != null) {
				try {					
					return new ResponseEntity<List<CheckLog>>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getContactSite Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<List<CheckLog>>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<List<CheckLog>>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<List<CheckLog>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}		

	@CrossOrigin(origins = "*")
	@Operation(summary = "get List data check log by visitId")	
	@RequestMapping(value = "/getListCheckLogByVisitId/{p_visit_id}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<List<CheckLog>> _getListCheckLogByVisitId(@PathVariable Integer p_visit_id){		
		List<CheckLog> result;		
		try {	
			result = checkLogDb.getListCheckLogByVisitId(p_visit_id); 
			if(result != null) {
				try {					
					return new ResponseEntity<List<CheckLog>>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getContactSite Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<List<CheckLog>>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<List<CheckLog>>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<List<CheckLog>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// public CheckLog setDoCheckLog(VisitCheckLogRequest p_obj_checkLogReq);
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "Do Check Log")	
	@RequestMapping(value = "/setDoCheckLog", method = RequestMethod.POST, produces = "application/json")	
	public ResponseEntity<CheckLog> _setDoCheckLog(@RequestBody VisitCheckLogRequest p_obj_checklogReq){		
		CheckLog result;		
		try {	
			result = checkLogDb.setDoCheckLog(p_obj_checklogReq);
			if(result != null) {
				try {					
					return new ResponseEntity<CheckLog>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<CheckLog>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<CheckLog>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<CheckLog>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
