package com.gudanggaramtbk.server.controller;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.gudanggaramtbk.server.dao.guestDao;
import com.gudanggaramtbk.server.model.Guest;
import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping(value = "/ggguestbook/guest/")
public class GuestController {
	@Resource 
	guestDao 	guestDb;
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "get data guest by id")	
	@RequestMapping(value = "/getGuestById/{p_guestId}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<Guest> _getGuestById(@PathVariable Integer p_guestId){		
		Guest result;		
		try {	
			result = guestDb.getGuestById(p_guestId); 
			if(result != null) {
				try {					
					return new ResponseEntity<Guest>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getGuestById Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Guest>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Guest>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Guest>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "get data guest by card id")	
	@RequestMapping(value = "/getGuestCardId/{p_guestCardId}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<Guest> _getGuestCardId(@PathVariable String p_guestCardId){		
		Guest result;
		Guest noresult; 
		noresult = new Guest(); 
		noresult.setGuestId(-1); 
		noresult.setGuestCardId(p_guestCardId); 
		
		try {	
			result = guestDb.getGuestCardId(p_guestCardId); 
			if(result != null) {
				try {					
					return new ResponseEntity<Guest>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getGuestCardId Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Guest>(result, HttpStatus.OK);				
			}
			else {
				//return new ResponseEntity<Guest>(noresult, HttpStatus.NO_CONTENT);								
				return new ResponseEntity<Guest>(noresult, HttpStatus.OK);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Guest>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	

	@CrossOrigin(origins = "*")
	@Operation(summary = "get data list guest")	
	@RequestMapping(value = "/getListGuest", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<List<Guest>> _getListGuest(){		
		List<Guest> result;		
		try {	
			result = guestDb.getListGuest(); 
			if(result != null) {
				try {					
					return new ResponseEntity<List<Guest>>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getListGuest Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<List<Guest>>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<List<Guest>>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<List<Guest>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}		

	@CrossOrigin(origins = "*")
	@Operation(summary = "Save data Guest")	
	@RequestMapping(value = "/saveGuestInfo", method = RequestMethod.POST, produces = "application/json")	
	public ResponseEntity<Guest> _saveGuestInfo(@RequestBody Guest p_obj_guest){		
		Guest result;		
		try {	
			result = guestDb.saveGuestInfo(p_obj_guest);
			if(result != null) {
				try {					
					return new ResponseEntity<Guest>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_saveGuestInfo Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Guest>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Guest>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Guest>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
}

