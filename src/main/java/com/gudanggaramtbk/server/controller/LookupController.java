package com.gudanggaramtbk.server.controller;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.gudanggaramtbk.server.dao.lookupDao;
import com.gudanggaramtbk.server.dao.siteDao;
import com.gudanggaramtbk.server.model.Lookup;
import com.gudanggaramtbk.server.model.Site;
import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping(value = "/ggguestbook/lookup/")
public class LookupController {
	@Resource 
	lookupDao 	lookupDb;
	 

		@CrossOrigin(origins = "*")
		@Operation(summary = "get data lookup by id")	
		@RequestMapping(value = "/getlookupyId/{p_lookupId}", method = RequestMethod.GET, produces = "application/json")	
		public ResponseEntity<Lookup> _getlookupyId(@PathVariable Integer p_lookupId){		
			Lookup result;		
			
			try {	
				result = lookupDb.getlookupyId(p_lookupId); 
				if(result != null) {
					try {					
						return new ResponseEntity<Lookup>(result, HttpStatus.OK);									
					}
					catch(Exception e) {
						e.getStackTrace(); 
						System.out.println("_getlookupyId Exception -> " + e.getMessage());					
					}				
					return new ResponseEntity<Lookup>(result, HttpStatus.OK);				
				}
				else {
					return new ResponseEntity<Lookup>(result, HttpStatus.NO_CONTENT);								
				}
			}
			catch(Exception e) {
				return new ResponseEntity<Lookup>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}	
		
		
		@CrossOrigin(origins = "*")
		@Operation(summary = "get data list lookup by group")	
		@RequestMapping(value = "/getListLookupByGroup/{p_group}", method = RequestMethod.GET, produces = "application/json")	
		public ResponseEntity<List<Lookup>> _getListLookupByGroup(@PathVariable String p_group){		
			List<Lookup> result;		
			try {	
				result = lookupDb.getListLookupByGroup(p_group); 
				if(result != null) {
					try {					
						return new ResponseEntity<List<Lookup>>(result, HttpStatus.OK);									
					}
					catch(Exception e) {
						e.getStackTrace(); 
						System.out.println("_getListLookupByGroup Exception -> " + e.getMessage());					
					}				
					return new ResponseEntity<List<Lookup>>(result, HttpStatus.OK);				
				}
				else {
					return new ResponseEntity<List<Lookup>>(result, HttpStatus.NO_CONTENT);								
				}
			}
			catch(Exception e) {
				return new ResponseEntity<List<Lookup>>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}		
	}

