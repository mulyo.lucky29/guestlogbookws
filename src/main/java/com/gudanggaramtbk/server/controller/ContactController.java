package com.gudanggaramtbk.server.controller;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.gudanggaramtbk.server.dao.contactDao;
import com.gudanggaramtbk.server.model.Contact;
import com.gudanggaramtbk.server.model.Guest;
import com.gudanggaramtbk.server.model.request.ContactSaveRequest;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping(value = "/ggguestbook/contact/")
public class ContactController {
	@Resource 
	contactDao 	contactDb;

	@CrossOrigin(origins = "*")
	@Operation(summary = "get data contact by id")	
	@RequestMapping(value = "/getContact/{p_contactId}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<Contact> _getContact(@PathVariable Integer p_contactId){		
		Contact result;		
		try {	
			result = contactDb.getContact(p_contactId); 
			if(result != null) {
				try {					
					return new ResponseEntity<Contact>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getContact Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Contact>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Contact>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Contact>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "get data contact for site")	
	@RequestMapping(value = "/getContactSite/{p_siteCode}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<List<Contact>> _getListContactBySite(@PathVariable String p_siteCode){		
		List<Contact> result;		
		try {	
			result = contactDb.getListContactBySite(p_siteCode); 
			if(result != null) {
				try {					
					return new ResponseEntity<List<Contact>>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getContactSite Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<List<Contact>>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<List<Contact>>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<List<Contact>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}		

	@CrossOrigin(origins = "*")
	@Operation(summary = "Save data Contact")	
	@RequestMapping(value = "/saveContact", method = RequestMethod.POST, produces = "application/json")	
	public ResponseEntity<Contact> _saveContact(@RequestBody ContactSaveRequest p_obj_contact){		
		Contact result;		
		try {	
			result = contactDb.saveContact(p_obj_contact);
			if(result != null) {
				try {					
					return new ResponseEntity<Contact>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_saveContact Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Contact>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Contact>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Contact>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin(origins = "*")
	@Operation(summary = "Delete data Contact")	
	@RequestMapping(value = "/{p_contactId}", method = RequestMethod.DELETE, produces = "application/json")	
	public ResponseEntity<Contact> _deleteContact(@PathVariable Integer p_contactId){		
		Contact result;		
		try {	
			result = contactDb.deleteContact(p_contactId);
			if(result != null) {
				try {					
					return new ResponseEntity<Contact>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_deleteContact Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Contact>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Contact>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Contact>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
