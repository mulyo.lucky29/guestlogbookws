package com.gudanggaramtbk.server.controller;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.gudanggaramtbk.server.dao.siteDao;
import com.gudanggaramtbk.server.dao.visitCounterDao;
import com.gudanggaramtbk.server.model.Site;
import com.gudanggaramtbk.server.model.VisitCounter;
import io.swagger.v3.oas.annotations.Operation;

 
@RestController
@RequestMapping(value = "/ggguestbook/visitcounter/")
public class VisitCounterController {
	@Resource 
	visitCounterDao 	vcounterDb;	
	@Resource 	
	siteDao				siteDb;

	private VisitCounter initVisitCounter(Integer p_siteId, String p_date) {
		VisitCounter result; 
		Site blankSite; 		
		blankSite = siteDb.getSitebyId(p_siteId); 	
		
		result = new VisitCounter(); 
		result.setSiteId(blankSite.getSiteId());
		result.setSiteCode(blankSite.getSiteCode()); 
		result.setSiteName(blankSite.getSiteName()); 
		result.setVisitDate(p_date);
		result.setCpersonVisit(0); 
		result.setCpersonActiveVisit(0); 
		result.setCpersonEndedNormalVisit(0); 
		result.setCpersonEndedCancelledVisit(0); 
		result.setCsessionVisit(0); 
		result.setCsessionActiveVisit(0);
		result.setCsessionEndedNormalVisit(0); 
		result.setCsessionEndedCancelledVisit(0); 

		return result; 
 }
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "get visit counter in site by date")	
	@RequestMapping(value = "/getVisitCounter/{p_siteId}/{p_date}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<VisitCounter> _getVisitCounter(@PathVariable Integer p_siteId, @PathVariable String p_date){		
		VisitCounter result;		
				
		try {	
			result = vcounterDb.getVisitCounter(p_siteId, p_date); 
			if(result != null) {
				try {					
					return new ResponseEntity<VisitCounter>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					result = initVisitCounter(p_siteId, p_date); 
					e.getStackTrace(); 
				}				
				return new ResponseEntity<VisitCounter>(result, HttpStatus.OK);						
			}
			else {	
				// no content 
				result = initVisitCounter(p_siteId, p_date); 
				return new ResponseEntity<VisitCounter>(result, HttpStatus.OK);						
			}
		}
		catch(Exception e) {			
			result = initVisitCounter(p_siteId,p_date); 
			return new ResponseEntity<VisitCounter>(result, HttpStatus.OK);		
		}
	}	
	
}