package com.gudanggaramtbk.server.controller;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.gudanggaramtbk.server.dao.applicationDao;
import com.gudanggaramtbk.server.dao.siteDao;
import com.gudanggaramtbk.server.model.Application;
import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping(value = "/ggguestbook/application/")
public class ApplicationController {
	@Resource 
	applicationDao 	appDb;

	 
	@CrossOrigin(origins = "*")
	@Operation(summary = "get application settings")	
	@RequestMapping(value = "/getAppSetting", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<Application> _getAppSetting(){		
		Application result;		
		
		try {	
			result = appDb.getAppSetting(); 
			if(result != null) {
				try {					
					return new ResponseEntity<Application>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getAppSetting Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Application>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Application>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Application>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	

}