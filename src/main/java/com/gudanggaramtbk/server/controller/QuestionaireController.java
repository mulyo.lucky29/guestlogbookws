package com.gudanggaramtbk.server.controller;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.gudanggaramtbk.server.dao.lookupDao;
import com.gudanggaramtbk.server.dao.questionaireDao;
import com.gudanggaramtbk.server.model.Questionaire;
import com.gudanggaramtbk.server.model.QuestionaireH;
import com.gudanggaramtbk.server.model.request.QuestionaireHSaveRequest;
import com.gudanggaramtbk.server.model.request.QuestionaireSaveRequest;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping(value = "/ggguestbook/questionaire/")
public class QuestionaireController {
	@Resource 
	questionaireDao 	questionaireDb;

	@CrossOrigin(origins = "*")
	@Operation(summary = "get list point questionaire by id")	
	@RequestMapping(value = "/getListQuestionaireById/{p_queId}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<List<Questionaire>> _getListQuestionaireById(@PathVariable Integer p_queId){		
		List<Questionaire> result;		
		try {	
			result = questionaireDb.getListQuestionaireById(p_queId); 
			if(result != null) {
				try {					
					return new ResponseEntity<List<Questionaire>>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getListLookupByGroup Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<List<Questionaire>>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<List<Questionaire>>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<List<Questionaire>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

	@CrossOrigin(origins = "*")
	@Operation(summary = "set save point questionaire")	
	@RequestMapping(value = "/savePoint", method = RequestMethod.POST, produces = "application/json")		
	public ResponseEntity<Questionaire> _saveQuestionPoint(@RequestBody QuestionaireSaveRequest p_queSave){		
		Questionaire result;	
		try {				    	
			result = questionaireDb.saveQuestionPoint(p_queSave);	
			if(result != null) {
				try {					
					return new ResponseEntity<Questionaire>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_saveQuestionPoint Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Questionaire>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Questionaire>(result, HttpStatus.NO_CONTENT);								
			}			
	   }
	   catch(Exception e) {		   
		   return new ResponseEntity<Questionaire>(HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}

	@CrossOrigin(origins = "*")
	@Operation(summary = "delete point questionaire")	
	@RequestMapping(value = "/delete/{p_quelId}", method = RequestMethod.DELETE, produces = "application/json")	
	public ResponseEntity<Questionaire> _deleteQuestionPointById(@PathVariable Integer p_quelId){		
		Questionaire result;		
		try {	
			result = questionaireDb.deleteQuestionPointById(p_quelId);
			if(result != null) {
				try {					
					return new ResponseEntity<Questionaire>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_deleteQuestionPointById Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Questionaire>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Questionaire>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Questionaire>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
}
