package com.gudanggaramtbk.server.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.gudanggaramtbk.server.dao.siteDao;
import com.gudanggaramtbk.server.model.DeskUser;
import com.gudanggaramtbk.server.model.Guest;
import com.gudanggaramtbk.server.model.Site;
import com.gudanggaramtbk.server.model.request.DeskUserLoginSaveRequest;
import com.gudanggaramtbk.server.model.request.SiteSaveRequest;

import io.swagger.v3.oas.annotations.Operation;

 
@RestController
@RequestMapping(value = "/ggguestbook/site/")
public class SiteController {
	@Resource 
	siteDao 	siteDb;

	 
	@CrossOrigin(origins = "*")
	@Operation(summary = "get data site by id")	
	@RequestMapping(value = "/getSiteById/{p_siteId}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<Site> _getSiteById(@PathVariable Integer p_siteId){		
		Site result;		
		
		try {	
			result = siteDb.getSitebyId(p_siteId); 
			if(result != null) {
				try {					
					return new ResponseEntity<Site>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getSite Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Site>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Site>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Site>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "get data site by code")	
	@RequestMapping(value = "/getSiteByCode/{p_siteCode}", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<Site> _getSiteByCode(@PathVariable String p_siteCode){		
		Site result;		
		Site noresult; 
		noresult = new Site(); 
		noresult.setSiteId(-1); 
		noresult.setSiteCode(p_siteCode);
		
		try {	
			result = siteDb.getSitebyCode(p_siteCode); 
			if(result != null) {
				try {					
					return new ResponseEntity<Site>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getSite Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Site>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Site>(noresult, HttpStatus.OK);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<Site>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "get data list site")	
	@RequestMapping(value = "/getListSite", method = RequestMethod.GET, produces = "application/json")	
	public ResponseEntity<List<Site>> _getListSite(){		
		List<Site> result;		
		try {	
			result = siteDb.getListSite(); 
			if(result != null) {
				try {					
					return new ResponseEntity<List<Site>>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_getListSite Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<List<Site>>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<List<Site>>(result, HttpStatus.NO_CONTENT);								
			}
		}
		catch(Exception e) {
			return new ResponseEntity<List<Site>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@CrossOrigin(origins = "*")
	@Operation(summary = "set save Site")	
	@RequestMapping(value = "/saveSite", method = RequestMethod.POST, produces = "application/json")		
	public ResponseEntity<Site> _saveSite(@RequestBody SiteSaveRequest p_siteSaveObj){		
		Site result;	
		try {				    	
			result = siteDb.saveSite(p_siteSaveObj);	
			if(result != null) {
				try {					
					return new ResponseEntity<Site>(result, HttpStatus.OK);									
				}
				catch(Exception e) {
					e.getStackTrace(); 
					System.out.println("_saveSite Exception -> " + e.getMessage());					
				}				
				return new ResponseEntity<Site>(result, HttpStatus.OK);				
			}
			else {
				return new ResponseEntity<Site>(result, HttpStatus.NO_CONTENT);								
			}			
	   }
	   catch(Exception e) {		   
		   return new ResponseEntity<Site>(HttpStatus.INTERNAL_SERVER_ERROR);
	   }
	}

}