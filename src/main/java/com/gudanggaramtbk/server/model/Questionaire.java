package com.gudanggaramtbk.server.model;

import lombok.Data;

@Data
public class Questionaire {
	Integer quehid;
	Integer quedid;
	Integer quelineno;	
	String quetext;
	String quehname;
	String quehdesc;
}
