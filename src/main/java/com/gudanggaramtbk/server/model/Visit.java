package com.gudanggaramtbk.server.model;

import lombok.Data;
import com.gudanggaramtbk.server.model.Guest;
import com.gudanggaramtbk.server.model.Contact;
import com.gudanggaramtbk.server.model.Site;

@Data
public class Visit {
	Integer visitId;
	String visitSessionCode;
	String visitDate;
	String visitPurpose;
	String checkIn; 
	String checkOut;
	String startVisitDate; 
	String endVisitDate; 
	String endVisitFlag;
	String endVisitReason; 
	String endVisitBy; 
	Guest objGuest; 
	Site objSite; 
	Contact objContact; 
	String assuranceCardType;
	String assuranceCardNo;
	String accessCardTag;
	String accessCardPairNo; 	
	String formaccessment; 
}
