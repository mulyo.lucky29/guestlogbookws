package com.gudanggaramtbk.server.model;

import lombok.Data;
/*
CREATE TABLE gggg.m_contact (
		contactId numeric NOT NULL DEFAULT nextval('gggg.m_contactId_seq'),
		siteCode varchar(50) NOT NULL,
		siteSpecific varchar(500) NOT NULL, 
		contactName varchar(500) NOT NULL,	
		contactEmail varchar(500) NULL,
		contactExt varchar(50) NOT NULL,	
		creation_date timestamp(0) NULL,
		created_by varchar(500) NULL,
		last_update_date timestamp(0) NULL,
		last_updated_by varchar(500) NULL,
		CONSTRAINT m_contact_unique UNIQUE (contactExt)
	);
*/ 

@Data
public class Contact {
	Integer contactId;
	String siteCode;
	String siteSpecific;
	String tenantCompany;
	String contactType;
	String contactName;	
	String contactPhone; 
	String contactEmail;	
	String contactExt;	
	String contactAltName;
}
