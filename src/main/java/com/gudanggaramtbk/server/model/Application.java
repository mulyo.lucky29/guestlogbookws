package com.gudanggaramtbk.server.model;

import lombok.Data;

 
@Data
public class Application {
	String appsname;
	String appsdescription;
	String appversion;
	String apiversion;
	String appscredit;
	String appsmobileurl;
	String appsstatus;	
}
