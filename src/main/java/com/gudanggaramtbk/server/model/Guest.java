package com.gudanggaramtbk.server.model;

import lombok.Data;

/*
  CREATE TABLE gggg.m_guest (
	guestId numeric NOT NULL DEFAULT nextval('gggg.m_guestId_seq'),
	guestCardId varchar(50) NOT NULL,
	guestName varchar(500) NOT NULL,
	guestPhone varchar(50) NOT NULL,
	guestEmail varchar(500) NOT NULL,
	guestCompany varchar(500) NOT NULL,
	guestPictureUrl varchar(2500) NULL,
	guestPictureCardId varchar(2500) NULL,
	isActive varchar(1) NOT null,
	creation_date timestamp(0) NULL,
	created_by varchar(500) NULL,
	last_update_date timestamp(0) NULL,
	last_updated_by varchar(500) NULL,
	CONSTRAINT m_guest_unique UNIQUE (guestCardId)
);

 */
@Data
public class Guest {
	Integer guestId;
	String guestCardId;
	String guestName;
	String guestPhone;
	String guestEmail;
	String guestCompany;
	String guestPictureUrl;
	String guestPictureCardId;
	String isActive;	
}
