package com.gudanggaramtbk.server.model;

import lombok.Data;

/*
 CREATE TABLE gggg.m_site (
	siteId numeric NOT NULL DEFAULT nextval('gggg.m_siteId_seq'),
	siteCode varchar(50) NOT NULL,
	siteName varchar(50) NULL,
	isActive varchar(1) NOT NULL,
	creation_date timestamp(0) NULL,
	created_by varchar(50) NULL,
	last_update_date timestamp(0) NULL,
	last_updated_by varchar(50) NULL,
	CONSTRAINT m_site_unique UNIQUE (siteCode)
);

 */

@Data
public class Site {
	Integer siteId;
	String siteCode;
	String siteName;
	String isActive;
	Integer formaccessmentid; 
}
