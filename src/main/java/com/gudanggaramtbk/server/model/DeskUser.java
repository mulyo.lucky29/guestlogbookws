package com.gudanggaramtbk.server.model;

import lombok.Data;

@Data
public class DeskUser {
	Integer userId;
	String username;
	String phonenumber;
	String emailaddress;
	Site objSite; 	
	String isAdmin;
	String isActive;	
}
