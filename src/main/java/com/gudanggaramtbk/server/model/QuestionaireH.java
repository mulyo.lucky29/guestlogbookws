package com.gudanggaramtbk.server.model;

import lombok.Data;

@Data
public class QuestionaireH {
	Integer quehid;
	String quehname;
	String quehdesc;
}
