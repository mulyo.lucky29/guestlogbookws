package com.gudanggaramtbk.server.model;

import lombok.Data;

@Data
public class Lookup {
	Integer lookupId;
	String lookupGroupCode; 
	String lookupValue; 
	String lookupDescription; 
	Integer lookupOrderNo; 
	String isActive;
}
