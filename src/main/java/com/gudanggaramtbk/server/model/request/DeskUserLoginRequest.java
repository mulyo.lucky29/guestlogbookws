package com.gudanggaramtbk.server.model.request;

import lombok.Data;

@Data
public class DeskUserLoginRequest {
	String dskusername; 
	String dskpassword;
}