package com.gudanggaramtbk.server.model.request;


import lombok.Data;

@Data
public class DeskUserLoginResetRequest {
	String dskusername; 
	String dskpassword; 
	String dskCreatedBy; 
}
