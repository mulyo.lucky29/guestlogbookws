package com.gudanggaramtbk.server.model.request;

import lombok.Data;

@Data
public class QuestionaireHSaveRequest {
	Integer quehid;
	String quehname;
	String quehdesc;
	String createdBy; 
}
