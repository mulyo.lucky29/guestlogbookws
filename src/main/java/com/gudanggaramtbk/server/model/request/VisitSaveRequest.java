package com.gudanggaramtbk.server.model.request;

import lombok.Data;

@Data
public class VisitSaveRequest {
	Integer siteid;
	Integer guestid; 
	Integer contactid; 
	String visitpurpose; 
	String contactaltname;
	String assuranceCardType; 
	String assuranceCardNo; 
	String formaccessment; 	 
}

