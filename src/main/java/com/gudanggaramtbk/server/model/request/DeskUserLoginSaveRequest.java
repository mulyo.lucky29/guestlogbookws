package com.gudanggaramtbk.server.model.request;

import lombok.Data;

@Data
public class DeskUserLoginSaveRequest {
	String dskusername; 
	String dskPhoneNo; 
	String dskEmail;	
	Integer dsksiteId; 
	String dskisAdmin; 
	String dskisActive; 	
	String dskCreatedBy; 
}
