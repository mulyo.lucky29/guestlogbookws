package com.gudanggaramtbk.server.model.request;

import lombok.Data;

@Data
public class VisitCheckLogRequest {
	String visitSessionCode; 
	String terminalId; 
}

