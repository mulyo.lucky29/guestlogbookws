package com.gudanggaramtbk.server.model.request;

import lombok.Data;

@Data
public class QuestionaireSaveRequest {
	Integer quehid;
	Integer quedid;
	Integer quelineno;	
	String quetext;	
	String createdBy; 
}
