package com.gudanggaramtbk.server.model.request;

import lombok.Data;

@Data
public class ContactSaveRequest {
	Integer contactId;
	String siteCode;
	String siteSpecific;
	String tenantCompany;
	String contactType;
	String contactName;	
	String contactPhone; 
	String contactEmail;	
	String contactExt;	
	String username;
}
