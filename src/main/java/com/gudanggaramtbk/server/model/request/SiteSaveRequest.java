package com.gudanggaramtbk.server.model.request;

import lombok.Data;

@Data
public class SiteSaveRequest {
	String siteCode;
	String siteName;
	String isActive;
	Integer formaccessmentid; 
	String createdBy; 
}
