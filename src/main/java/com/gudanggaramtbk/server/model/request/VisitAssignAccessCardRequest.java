package com.gudanggaramtbk.server.model.request;

import lombok.Data;
@Data
public class VisitAssignAccessCardRequest {
	Integer VisitId;
	String accessCardTag;
	String accessCardPairNo; 
	String username;
}
