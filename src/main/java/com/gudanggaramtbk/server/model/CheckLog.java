package com.gudanggaramtbk.server.model;

import lombok.Data;

@Data
public class CheckLog {
	Integer checkLogId;
	Integer visitId; 
	String visitSessionCode; 
	String checkedTime; 
	String checkTyped;  
	String checkedTerminalId;	
}

