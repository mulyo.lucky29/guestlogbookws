package com.gudanggaramtbk.server.model;

import lombok.Data;

@Data
public class VisitCounter {
	Integer siteId;
	String siteCode;
	String siteName;	
	String visitDate;
	Integer cpersonVisit;
	Integer cpersonActiveVisit;
	Integer cpersonEndedNormalVisit; 
	Integer cpersonEndedCancelledVisit;
	Integer cpersonVisitExpired; 
	Integer csessionVisit;
	Integer csessionActiveVisit;
	Integer csessionEndedNormalVisit; 
	Integer csessionEndedCancelledVisit; 
	Integer csessionVisitExpired; 
}
