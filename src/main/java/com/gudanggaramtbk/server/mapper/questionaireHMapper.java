package com.gudanggaramtbk.server.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gudanggaramtbk.server.model.QuestionaireH;

public class questionaireHMapper implements RowMapper<QuestionaireH> {
	@Override
	public QuestionaireH mapRow(ResultSet rs, int rowNum) throws SQLException {	    		    		
		QuestionaireH queh = new QuestionaireH();
		queh.setQuehid(rs.getInt("quehid"));
		queh.setQuehname(rs.getString("quehname"));
		queh.setQuehdesc(rs.getString("quehdesc"));
		
		return queh;
	}
}