package com.gudanggaramtbk.server.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.gudanggaramtbk.server.model.Contact;

public class contactMapper implements RowMapper<Contact> {	
	
	@Override
	public Contact mapRow(ResultSet rs, int rowNum) throws SQLException {	    		    		
		Contact ca = new Contact();
		ca.setContactId(rs.getInt("contactid"));
		ca.setSiteCode(rs.getString("sitecode"));
		ca.setSiteSpecific(rs.getString("sitespecific"));
		ca.setTenantCompany(rs.getString("tenantcompany"));
		ca.setContactType(rs.getString("contactType"));
		ca.setContactName(rs.getString("contactname"));
		ca.setContactEmail(rs.getString("contactemail"));
		ca.setContactPhone(rs.getString("contactPhone"));
		ca.setContactExt(rs.getString("contactext"));	
		//ca.setContactAltName(rs.getString("contactAltName"));
		return ca;
	}
}