package com.gudanggaramtbk.server.mapper;


/*
		 String queryBase = "select vs.visitId,"
		 		+ "	   vs.visitSessionCode,"
		 		+ "	   vs.visitDate,"
		 		+ "	   vs.visitPurpose,"
		 		
		 		+ "	   vs.guestId,"
		 		+ "	   vs.guestCardId,"
		 		+ "	   vs.guestName,"
		 		+ "	   vs.guestPhone,"
		 		+ "	   vs.guestEmail,"
		 		+ "	   vs.guestCompany,"		 		
		 		+ "	   vs.guestPictureUrl,"
		 		+ "	   vs.guestPictureCardId,"

		 		+ "	   vs.siteId,"
		 		+ "	   vs.siteCode,"
		 		+ "	   vs.siteName,"
		 		
		 		+ "	   vs.contactId,"
		 		+ "	   vs.contactName,"
		 		+ "	   vs.contactEmail,"
		 		+ "	   vs.contactExt,"
		 		+ "	   vs.contactAltName,"
		 		
		 		+ "	   vs.checkIn,"
		 		+ "	   vs.checkOut,"
		 		+ "	   vs.cancelledFlag,"
		 		+ "	   vs.cancelledReason "
		 		+ "from gggg.m_visit vs "
		 		+ "where 1 = 1 "; 
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gudanggaramtbk.server.model.Contact;
import com.gudanggaramtbk.server.model.Guest;
import com.gudanggaramtbk.server.model.Site;
import com.gudanggaramtbk.server.model.Visit;

public class visitMapper implements RowMapper<Visit> {	

	@Override
	public Visit mapRow(ResultSet rs, int rowNum) throws SQLException {	    		    		
		Visit vs = new Visit();
		vs.setVisitId(rs.getInt("visitId"));
		vs.setVisitSessionCode(rs.getString("visitSessionCode"));
		vs.setVisitDate(rs.getString("visitDate"));
		vs.setVisitPurpose(rs.getString("visitPurpose"));		
		// prepare for data Guest
		Guest gs = new Guest(); 
		if(rs.getString("guestId") != null) {	
			gs.setGuestId(rs.getInt("guestId"));
			gs.setGuestCardId(rs.getString("guestCardId"));
			gs.setGuestName(rs.getString("guestName"));
			gs.setGuestPhone(rs.getString("guestPhone"));
			gs.setGuestEmail(rs.getString("guestEmail"));
			gs.setGuestCompany(rs.getString("guestCompany"));
			gs.setGuestPictureUrl(rs.getString("guestPictureUrl"));
			gs.setGuestPictureCardId(rs.getString("guestPictureCardId"));
		}		
		vs.setObjGuest(gs);
		// prepare for data Site 		
		Site si = new Site(); 
		if(rs.getString("siteId") != null) {	
			si.setSiteId(rs.getInt("siteId"));
			si.setSiteCode(rs.getString("siteCode"));
			si.setSiteName(rs.getString("siteName"));			
		}
		vs.setObjSite(si);
		// prepare for data contact 	
		Contact co = new Contact(); 
		if(rs.getString("contactName") != null) {	
			co.setContactId(rs.getInt("contactId"));
			co.setSiteCode(rs.getString("siteCode"));
			co.setSiteSpecific(rs.getString("sitespecific"));
			co.setTenantCompany(rs.getString("tenantcompany")); 
			co.setContactType(rs.getString("contacttype"));
			
			co.setContactName(rs.getString("contactName")); 
			co.setContactEmail(rs.getString("contactEmail")); 
			co.setContactPhone(rs.getString("contactphone")); 			
			co.setContactExt(rs.getString("contactExt"));
			co.setContactAltName(rs.getString("contactAltName")); 
		}
		else {
			co.setContactAltName(rs.getString("contactAltName")); 			
		}
		
		vs.setObjContact(co);
		vs.setCheckIn(rs.getString("checkIn"));
		vs.setCheckOut(rs.getString("checkOut"));
		vs.setStartVisitDate(rs.getString("startVisitDate"));
		vs.setEndVisitDate(rs.getString("endVisitDate"));
		vs.setEndVisitFlag(rs.getString("endVisitFlag"));
		vs.setEndVisitReason(rs.getString("endVisitReason"));
		vs.setEndVisitBy(rs.getString("endVisitBy"));
		vs.setAssuranceCardType(rs.getString("assuranceCardType"));
		vs.setAssuranceCardNo(rs.getString("assuranceCardNo"));
		vs.setAccessCardTag(rs.getString("accessCardTag"));
		vs.setAccessCardPairNo(rs.getString("accessCardPairNo"));
		vs.setFormaccessment(rs.getString("formaccessment")); 		
		
		return vs;
	}
}