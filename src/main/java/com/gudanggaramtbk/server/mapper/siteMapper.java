package com.gudanggaramtbk.server.mapper;

/*
 		 String queryBase = "select ms.siteId,"
		 		+ "       ms.siteCode,"
		 		+ "       ms.siteName,"
		 		+ "       ms.isActive "
		 		+ "from gggg.m_site ms "
		 		+ "where 1 = 1 "; 
 */


import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gudanggaramtbk.server.model.Site;

public class siteMapper implements RowMapper<Site> {	

	@Override
	public Site mapRow(ResultSet rs, int rowNum) throws SQLException {	    		    		
		Site si = new Site();
		si.setSiteId(rs.getInt("siteId"));
		si.setSiteCode(rs.getString("siteCode"));
		si.setSiteName(rs.getString("siteName"));
		si.setIsActive(rs.getString("isActive"));
		si.setFormaccessmentid(rs.getInt("formaccessmentid"));
		return si;
	}
}