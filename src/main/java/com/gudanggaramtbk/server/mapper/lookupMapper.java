package com.gudanggaramtbk.server.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gudanggaramtbk.server.model.Lookup;

public class lookupMapper implements RowMapper<Lookup> {	

	@Override
	public Lookup mapRow(ResultSet rs, int rowNum) throws SQLException {	    		    		
		Lookup lo = new Lookup();
		lo.setLookupId(rs.getInt("lookupId"));
		lo.setLookupGroupCode(rs.getString("lookupGroupCode"));
		lo.setLookupValue(rs.getString("lookupValue"));
		lo.setLookupDescription(rs.getString("lookupDescription"));
		lo.setLookupOrderNo(rs.getInt("lookupOrderNo"));
		lo.setIsActive(rs.getString("isActive"));
		
		return lo;
	}
}
