package com.gudanggaramtbk.server.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gudanggaramtbk.server.model.DeskUser;
import com.gudanggaramtbk.server.model.Site;

public class deskUserMapper implements RowMapper<DeskUser> {	
	@Override
	public DeskUser mapRow(ResultSet rs, int rowNum) throws SQLException {	    		    		
		DeskUser du = new DeskUser();
						
		du.setUserId(rs.getInt("userid"));
		du.setUsername(rs.getString("username"));		
		du.setPhonenumber(rs.getString("phonenumber"));		
		du.setEmailaddress(rs.getString("emailaddress"));	
		
		// prepare for site object 
		Site si = new Site(); 		
		si.setSiteId(rs.getInt("siteId"));
		si.setSiteCode(rs.getString("sitecode"));	
		si.setSiteName(rs.getString("sitename"));
		si.setFormaccessmentid(rs.getInt("formaccessmentid"));
		si.setIsActive(rs.getString("isSiteActive"));		

		du.setObjSite(si);
		
		du.setIsAdmin(rs.getString("isadmin"));
		du.setIsActive(rs.getString("isActive"));
		
		return du;
	}
}

