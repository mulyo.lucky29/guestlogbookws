package com.gudanggaramtbk.server.mapper;

/*
 		 String queryBase = "select mg.guestId,"
		 		+ "	   mg.guestCardId,"
		 		+ "	   mg.guestName,"
		 		+ "	   mg.guestPhone,"
		 		+ "	   mg.guestEmail,"
		 		+ "	   mg.guestCompany,"
		 		+ "	   mg.guestPictureUrl,"
		 		+ "	   mg.guestPictureCardId,"
		 		+ "	   mg.isActive "
		 		+ "from gggg.m_guest mg "
		 		+ "where 1 = 1 "; 

 */

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gudanggaramtbk.server.model.Guest;

public class guestMapper implements RowMapper<Guest> {	

	@Override
	public Guest mapRow(ResultSet rs, int rowNum) throws SQLException {	    		    		
		Guest gu = new Guest();
		gu.setGuestId(rs.getInt("guestId"));
		gu.setGuestCardId(rs.getString("guestCardId"));
		gu.setGuestName(rs.getString("guestName"));
		gu.setGuestPhone(rs.getString("guestPhone"));
		gu.setGuestEmail(rs.getString("guestEmail"));
		gu.setGuestCompany(rs.getString("guestCompany"));
		gu.setGuestPictureCardId(rs.getString("guestPictureUrl"));
		gu.setGuestPictureUrl(rs.getString("guestPictureCardId"));
		gu.setIsActive(rs.getString("isActive"));
		
		return gu;
	}
}
