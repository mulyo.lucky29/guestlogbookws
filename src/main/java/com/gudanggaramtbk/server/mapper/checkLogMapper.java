package com.gudanggaramtbk.server.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.gudanggaramtbk.server.model.CheckLog;


public class checkLogMapper implements RowMapper<CheckLog> {
	@Override
	public CheckLog mapRow(ResultSet rs, int rowNum) throws SQLException {	    		    		
		CheckLog cl = new CheckLog();
		cl.setCheckLogId(rs.getInt("checklogid"));
		cl.setVisitId(rs.getInt("visitid"));
		cl.setVisitSessionCode(rs.getString("visitsessioncode"));
		cl.setCheckedTime(rs.getString("checkedtime"));
		cl.setCheckTyped(rs.getString("checktyped"));
		cl.setCheckedTerminalId(rs.getString("checkedterminalid"));
		return cl;
	}
}