package com.gudanggaramtbk.server.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gudanggaramtbk.server.model.VisitCounter;

public class visitCounterMapper implements RowMapper<VisitCounter> {		
	@Override
	public VisitCounter mapRow(ResultSet rs, int rowNum) throws SQLException {	    		    		
		VisitCounter vc = new VisitCounter();
		try {
			vc.setSiteId(rs.getInt("sc_siteid"));
			vc.setSiteCode(rs.getString("sc_sitecode"));
			vc.setSiteName(rs.getString("sc_sitename"));
			vc.setVisitDate(rs.getString("sc_visitdate"));
			vc.setCpersonVisit(rs.getInt("sc_person_visit"));
		    vc.setCpersonActiveVisit(rs.getInt("sc_person_active_visit"));
			vc.setCpersonEndedNormalVisit(rs.getInt("sc_person_ended_normal_visit"));		
			vc.setCpersonEndedCancelledVisit(rs.getInt("sc_person_ended_cancelled_visit"));
			vc.setCpersonVisitExpired(rs.getInt("sc_person_visit_expired"));
							
			vc.setCsessionVisit(rs.getInt("sc_session_visit"));
			vc.setCsessionActiveVisit(rs.getInt("sc_session_active_visit"));
			vc.setCsessionEndedNormalVisit(rs.getInt("sc_session_ended_normal_visit"));
			vc.setCsessionEndedCancelledVisit(rs.getInt("sc_session_ended_cancelled_visit"));
			vc.setCsessionVisitExpired(rs.getInt("sc_session_visit_expired"));
						
		}
		catch(Exception e) {
			System.out.println("visitCounterMapper Exception ->" + e.getMessage()); 			
		}

		return vc;
	}
}
