package com.gudanggaramtbk.server.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gudanggaramtbk.server.model.Application;
import com.gudanggaramtbk.server.model.Site;

/*
 * 	 String queryBase = "select appsname,"
		 		+ "	   appsdescription,"
		 		+ "	   appversion,"
		 		+ "	   apiversion,"
		 		+ "	   appscredit,"
		 		+ "	   appsmobileurl,"
		 		+ "	   appsstatu s"
		 		+ "from gggg.m_apps "
		 		+ "limit 1 ";
 
 */

public class applicationMapper implements RowMapper<Application> {	

	@Override
	public Application mapRow(ResultSet rs, int rowNum) throws SQLException {	    		    		
		Application app = new Application();
		app.setAppsname(rs.getString("appsname"));
		app.setAppsdescription(rs.getString("appsdescription"));
		app.setAppversion(rs.getString("appversion"));
		app.setApiversion(rs.getString("apiversion"));
		app.setAppscredit(rs.getString("appscredit"));
		app.setAppsmobileurl(rs.getString("appsmobileurl"));
		app.setAppsstatus(rs.getString("appsstatus"));
		
		return app;
	}
}