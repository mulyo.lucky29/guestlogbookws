package com.gudanggaramtbk.server.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gudanggaramtbk.server.model.Questionaire;


public class questionaireMapper implements RowMapper<Questionaire> {
	@Override
	public Questionaire mapRow(ResultSet rs, int rowNum) throws SQLException {	    		    		
		Questionaire que = new Questionaire();
		que.setQuehid(rs.getInt("quehid"));
		que.setQuedid(rs.getInt("quedid"));
		que.setQuelineno(rs.getInt("quelineno"));
		que.setQuetext(rs.getString("quetext"));
		que.setQuehname(rs.getString("quehname"));
		que.setQuehdesc(rs.getString("quehdesc"));
		
		return que;
	}
}
