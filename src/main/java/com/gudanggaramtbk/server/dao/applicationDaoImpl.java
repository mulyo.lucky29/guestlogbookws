package com.gudanggaramtbk.server.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.gudanggaramtbk.server.mapper.applicationMapper;
import com.gudanggaramtbk.server.model.Application;


@Repository
public class applicationDaoImpl implements applicationDao {	 
	 @Autowired
	    JdbcTemplate ds;
		 
		 String queryBase = "select appsname,"
		 		+ "	   appsdescription,"
		 		+ "	   appversion,"
		 		+ "	   apiversion,"
		 		+ "	   appscredit,"
		 		+ "	   appsmobileurl,"
		 		+ "	   appsstatus "
		 		+ "from gggg.m_apps "
		 		+ "limit 1 ";
 
	 
		 public Application getAppSetting(){
			 	Application result; 
						  
				 final String strQuery = queryBase; 				   
				 
				 try {		
					  result = (Application) ds.queryForObject(strQuery, new applicationMapper());						
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
			 } 
}