package com.gudanggaramtbk.server.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.gudanggaramtbk.server.mapper.guestMapper;
import com.gudanggaramtbk.server.model.Guest;

/*
 CREATE TABLE gggg.m_guest (
	guestId numeric NOT NULL DEFAULT nextval('gggg.m_guestId_seq'),
	guestCardId varchar(50) NOT NULL,
	guestName varchar(500) NOT NULL,
	guestPhone varchar(50) NOT NULL,
	guestEmail varchar(500) NOT NULL,
	guestCompany varchar(500) NOT NULL,
	guestPictureUrl varchar(2500) NULL,
	guestPictureCardId varchar(2500) NULL,
	isActive varchar(1) NOT null,
	creation_date timestamp(0) NULL,
	created_by varchar(500) NULL,
	last_update_date timestamp(0) NULL,
	last_updated_by varchar(500) NULL,
	CONSTRAINT m_guest_unique UNIQUE (guestCardId)
);

 */
	 
@Repository
public class guestDaoImpl implements guestDao  {
	 @Autowired 
	 NamedParameterJdbcTemplate dx; 
	
	 @Autowired
	 JdbcTemplate ds;
		 
		 String queryBase = "select mg.guestId,"
		 		+ "	   mg.guestCardId,"
		 		+ "	   mg.guestName,"
		 		+ "	   mg.guestPhone,"
		 		+ "	   mg.guestEmail,"
		 		+ "	   mg.guestCompany,"
		 		+ "	   mg.guestPictureUrl,"
		 		+ "	   mg.guestPictureCardId,"
		 		+ "	   mg.isActive "
		 		+ "from gggg.m_guest mg "
		 		+ "where 1 = 1 "; 

		 
		 public Guest getGuestById(Integer p_guestId){
				Guest result; 
						  
				 final String strQuery = queryBase 
						    + " and mg.guestId = ? "    
					 		+ " order by mg.guestId ";
				 
				 try {		
					  result = (Guest) ds.queryForObject(strQuery, new Object[]{p_guestId}, new guestMapper());						
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
			 } 

		 public Guest getGuestCardId(String p_guestCardId){
				Guest result; 
						  
				 final String strQuery = queryBase 
						    + " and mg.guestCardId = ? "
					 		+ " order by mg.guestId ";
				 
				 try {		
					  result = (Guest) ds.queryForObject(strQuery, new Object[]{p_guestCardId}, new guestMapper());						  
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
			 } 
		 
	 	 public List<Guest> getListGuest(){
				List<Guest> result; 
						  
				 final String strQuery = queryBase 
					 		+ " order by mg.guestId ";
				 
				 try {		
					  result = ds.query(strQuery, new guestMapper());			
					  if(result.isEmpty()) {				  
					  }
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
		}

	 	 public Guest saveGuestInfo(Guest p_obj_guest) {	
		    Guest result = new Guest(); 	    		
		    	
		    //	gggg.fn_save_guest(
			//		p_guestId in bigint,
			//		p_guestCardId in character varying, 
			//		p_guestName in character varying, 
			//		p_guestPhone in character varying, 
			//		p_guestEmail in character varying,
			//		p_guestCompany in character varying,
			//		p_guestPictureUrl in character varying,
			//		p_guestPictureCardId in character varying	
		 	//)
		    
		    try {	    			    	
		    	SqlParameterSource params = new MapSqlParameterSource()
		    			.addValue("p_guestId", p_obj_guest.getGuestId())
		    			.addValue("p_guestCardId", p_obj_guest.getGuestCardId())
		    			.addValue("p_guestName", p_obj_guest.getGuestName())
		    			.addValue("p_guestPhone", p_obj_guest.getGuestPhone())
		    			.addValue("p_guestEmail", p_obj_guest.getGuestEmail())
		    			.addValue("p_guestCompany", p_obj_guest.getGuestCompany())
		    			.addValue("p_guestPictureUrl", p_obj_guest.getGuestPictureUrl())
		    			.addValue("p_guestPictureCardId", p_obj_guest.getGuestPictureCardId());
		    			

		    	String strQuery = "select gggg.fn_save_guest("
						+ ":p_guestId,"
						+ ":p_guestCardId,"
						+ ":p_guestName,"
						+ ":p_guestPhone,"
						+ ":p_guestEmail," 
						+ ":p_guestCompany," 
						+ ":p_guestPictureUrl," 
						+ ":p_guestPictureCardId)"; 
						

		    	Integer guestId = dx.queryForObject(strQuery, params, int.class);	
		    	if(guestId == null) {
		    		result = null; 
		    	}
		    	else {
		    		result = getGuestById(guestId); 
		    	}
		    }
		    catch(Exception e) {
		    	e.printStackTrace();
		    	//log.debug("Exception : >" + e.toString());
		    	System.out.println("Exception : > " + e.getMessage().toString()); 
		    }
				    
			return result; 
		}

			 
		 
}
