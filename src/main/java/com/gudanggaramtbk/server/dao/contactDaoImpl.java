package com.gudanggaramtbk.server.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.gudanggaramtbk.server.mapper.contactMapper;
import com.gudanggaramtbk.server.model.Contact;
import com.gudanggaramtbk.server.model.Guest;
import com.gudanggaramtbk.server.model.request.ContactSaveRequest;

@Repository
public class contactDaoImpl implements contactDao  {
	 @Autowired
     JdbcTemplate ds;

	 @Autowired 
	 NamedParameterJdbcTemplate dx; 
	 
	 String queryBase = "select mc.contactid,"
	 		+ "	   mc.sitecode,"
	 		+ "	   mc.sitespecific,"
	 		+ "    mc.tenantcompany,"
	 		+ "    mc.contactType,"
	 		+ "	   mc.contactname,"
	 		+ "	   mc.contactemail,"
	 		+ "    mc.contactPhone," 	 		
	 		+ "	   mc.contactext "
	 		+ "from gggg.m_contact mc "
	 		+ "where 1 = 1 "; 
	 

	 public Contact getContact(Integer p_contactId){
		Contact result; 
				  
		 final String strQuery = queryBase 
				    + " and mc.contactType = 'Person' "
				    + " and mc.contactid = ? "    
			 		+ " order by mc.sitecode, mc.sitespecific, mc.contactname ";
		 
		 try {		
			  result = (Contact) ds.queryForObject(strQuery, new Object[]{p_contactId}, new contactMapper());						
			  return result; 
		  }
		  catch(Exception e) {
		    	e.printStackTrace();
		    	System.out.println("Exception : > " + e.getMessage().toString()); 
		    	return null; 
		  }		  
	 } 
		
	 public List<Contact> getListContactBySite(String p_siteCode){
		List<Contact> result; 
				  
		 final String strQuery = queryBase 
				    + " and mc.contactType = 'Person' "				 
				 	+ " and mc.sitecode = ? "   
			 		+ " order by mc.sitecode, mc.sitespecific, mc.contactname ";
		 
		 try {		
			  result = ds.query(strQuery,  new Object[] {p_siteCode}, new contactMapper());			

			  if(result.isEmpty()) {				  
			  }
			  return result; 
		  }
		  catch(Exception e) {
		    	e.printStackTrace();
		    	System.out.println("Exception : > " + e.getMessage().toString()); 
		    	return null; 
		  }		  
	} 

     public Contact saveContact(ContactSaveRequest p_obj_contact) {
    	Contact result = new Contact(); 	    		
    	
    	//gggg.fn_save_contact(
    	//		p_contactid in bigint, 
    	//		p_sitecode in character varying,
    	//		p_sitespecific in character varying,
    	//		p_tenantcompany in character varying,
    	//		p_contacttype in character varying,	
    	//		p_contactname in character varying, 
    	//		p_contactphone in character varying,
    	//		p_contactemail in character varying,
    	//		p_contactext in character varying,
    	//		p_user in character varying
    	//	 )
    	 	
	    try {	    			    	
	    	SqlParameterSource params = new MapSqlParameterSource()
	    			.addValue("p_contactid", p_obj_contact.getContactId())
	    			.addValue("p_sitecode", p_obj_contact.getSiteCode())
	    			.addValue("p_sitespecific", p_obj_contact.getSiteSpecific())
	    			.addValue("p_tenantcompany", p_obj_contact.getTenantCompany())
	    			.addValue("p_contacttype", p_obj_contact.getContactType())
	    			.addValue("p_contactname", p_obj_contact.getContactName())
	    			.addValue("p_contactphone", p_obj_contact.getContactPhone()) 
	    			.addValue("p_contactemail", p_obj_contact.getContactEmail())	    			
	    			.addValue("p_contactext", p_obj_contact.getContactExt())
	    			.addValue("p_user", p_obj_contact.getUsername()); 
	    			
	    	String strQuery = "select gggg.fn_save_contact("
					+ ":p_contactid,"
					+ ":p_sitecode,"
					+ ":p_sitespecific,"
					+ ":p_tenantcompany,"
					+ ":p_contacttype,"
					+ ":p_contactname," 
					+ ":p_contactphone,"
					+ ":p_contactemail,"					
					+ ":p_contactext,"
					+ ":p_user)"; 
					

	    	Integer contactId = dx.queryForObject(strQuery, params, int.class);	
	    	if(contactId == null) {
	    		result = null; 
	    	}
	    	else {
	    		result = getContact(contactId); 
	    	}
	    }
	    catch(Exception e) {
	    	e.printStackTrace();
	    	//log.debug("Exception : >" + e.toString());
	    	System.out.println("Exception : > " + e.getMessage().toString()); 
	    }
			    
		return result; 
    }

     public Contact deleteContact(Integer p_contactId) {
    	Contact result = new Contact(); 	    		
    	    	 	
    	// query first before deleted 
    	result = getContact(p_contactId);    	    	
	    try {	    			    	
	    	SqlParameterSource params = new MapSqlParameterSource()
	    			.addValue("p_contactid", p_contactId);
	    			
	    	String strQuery = "select gggg.fn_delete_contact(:p_contactid);"; 
					
	    	Integer deletedContactId = dx.queryForObject(strQuery, params, int.class);	
	    	if(deletedContactId == null) {
	    		result = null; 
	    	}
	    }
	    catch(Exception e) {
	    	e.printStackTrace();
	    	//log.debug("Exception : >" + e.toString());
	    	System.out.println("Exception : > " + e.getMessage().toString()); 
	    }
			    
		return result; 
    }

    
}