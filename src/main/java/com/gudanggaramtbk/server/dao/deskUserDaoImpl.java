package com.gudanggaramtbk.server.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.gudanggaramtbk.server.mapper.deskUserMapper;
import com.gudanggaramtbk.server.mapper.visitMapper;
import com.gudanggaramtbk.server.model.Contact;
import com.gudanggaramtbk.server.model.DeskUser;
import com.gudanggaramtbk.server.model.Guest;
import com.gudanggaramtbk.server.model.Visit;
import com.gudanggaramtbk.server.model.request.DeskUserLoginRequest;
import com.gudanggaramtbk.server.model.request.DeskUserLoginResetRequest;
import com.gudanggaramtbk.server.model.request.DeskUserLoginSaveRequest;


@Repository
public class deskUserDaoImpl implements deskUserDao {
	 @Autowired 
	 NamedParameterJdbcTemplate dx; 
	
	 @Autowired
	 JdbcTemplate ds;
		 
		 String queryBase = "select ml.userid "
		 		+ ",ml.username "
		 		+ ",ml.encryptedpassword "
		 		+ ",ml.phonenumber "
		 		+ ",ml.emailaddress "
		 		+ ",ml.siteid "
		 		+ ",ml.sitecode "
		 		+ ",ms.sitename "
		 		+ ",ms.isactive isSiteActive "
		 		+ ",ms.formaccessmentid "
		 		+ ",ml.isadmin "
		 		+ ",ml.isactive "
		 		+ "from gggg.m_login ml left join gggg.m_site ms on (ml.siteid = ms.siteid) "
		 		+ "where 1 = 1 "; 

		 public List<DeskUser> getListAllUser() {
				List<DeskUser> result; 
						  
				 final String strQuery = queryBase   
					 		+ " order by ml.userid";
				 
				 try {		
					  result = ds.query(strQuery, new deskUserMapper());			
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
		 } 

		 public List<DeskUser> getListUserBySite(Integer p_siteId) {
				List<DeskUser> result; 
						  
				 final String strQuery = queryBase 
						    + " and ml.siteid = ? "    
					 		+ " order by ml.userid";
				 
				 try {		
					  result = ds.query(strQuery, new Object[]{p_siteId}, new deskUserMapper());			
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
		 } 
		 
		 public DeskUser getUserById(Integer p_userId){
				DeskUser result; 
						  
				 final String strQuery = queryBase 
						    + " and ml.userid = ? "    
					 		+ " order by ml.userid ";
				 
				 try {		
					  result = (DeskUser) ds.queryForObject(strQuery, new Object[]{p_userId}, new deskUserMapper());						
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
		 } 
		 
		 public DeskUser getLogin(DeskUserLoginRequest p_loginReqObj) {				 
				DeskUser result = new DeskUser(); 	    		
		    	
			    //	gggg.fn_login(p_username character varying, p_password character varying)
			    
			    try {	    			    	
			    	SqlParameterSource params = new MapSqlParameterSource()
			    			.addValue("p_username", p_loginReqObj.getDskusername())
			    			.addValue("p_password", p_loginReqObj.getDskpassword());
			    			

			    	String strQuery = "select gggg.fn_login("
							+ ":p_username,"
							+ ":p_password)";							

			    	Integer userId = dx.queryForObject(strQuery, params, int.class);	
			    	if(userId == null) {
			    		result = null; 
			    	}
			    	else {
			    		result = getUserById(userId); 
			    	}
			    }
			    catch(Exception e) {
			    	e.printStackTrace();
			    	System.out.println("Exception : > " + e.getMessage().toString()); 
			    }
					    
				return result; 
		} 
		 
	 	 public DeskUser saveLogin(DeskUserLoginSaveRequest p_loginAccObj) {	
	 		 	DeskUser result = new DeskUser(); 	    		

	 		 	// gggg.fn_save_login(
	 		 	//		p_username in character varying,
	 		 	//		p_phoneNo in character varying,
	 		 	//		p_email	in character varying,
	 		 	//		p_siteid in bigint,
	 		 	//		p_isAdmin in character varying,	
	 		 	//		p_isactive in character varying,
	 		 	//		p_createdBy in character varying
	 		 	//	 )
	 		 				    
			    try {	    			    	
			    	SqlParameterSource params = new MapSqlParameterSource()
			    			.addValue("p_username", p_loginAccObj.getDskusername())
			    			.addValue("p_phoneNo", p_loginAccObj.getDskPhoneNo())
			    			.addValue("p_email", p_loginAccObj.getDskEmail())
			    			.addValue("p_siteid", p_loginAccObj.getDsksiteId())
			    			.addValue("p_isAdmin", p_loginAccObj.getDskisAdmin())
			    			.addValue("p_isactive", p_loginAccObj.getDskisActive())
			    			.addValue("p_createdBy", p_loginAccObj.getDskCreatedBy()); 
			    			    			
			    	String strQuery = "select gggg.fn_save_login("
							+ ":p_username,"
							+ ":p_phoneNo,"
							+ ":p_email,"
							+ ":p_siteid," 
							+ ":p_isAdmin," 
							+ ":p_isactive,"
							+ ":p_createdBy)";   

			    	Integer userId = dx.queryForObject(strQuery, params, int.class);	
			    	if(userId == null) {
			    		result = null; 
			    	}
			    	else {
			    		result = getUserById(userId); 
			    	}
			    }
			    catch(Exception e) {
			    	e.printStackTrace();
			    	System.out.println("Exception : > " + e.getMessage().toString()); 
			    }
					    
				return result; 
			}
	
	     public DeskUser deleteLogin(Integer p_userid) {
	    	 DeskUser result = new DeskUser(); 	    		
	     	    	 	
	     	// query first before deleted 
	     	result = getUserById(p_userid);    	    	
	 	    try {	    			    	
	 	    	SqlParameterSource params = new MapSqlParameterSource()
	 	    			.addValue("p_userid", p_userid);
	 	    			
	 	    	String strQuery = "select gggg.fn_delete_user(:p_userid);"; 
	 					
	 	    	Integer deletedUserId = dx.queryForObject(strQuery, params, int.class);	
	 	    	if(deletedUserId == null) {
	 	    		result = null; 
	 	    	}
	 	    }
	 	    catch(Exception e) {
	 	    	e.printStackTrace();
	 	    	//log.debug("Exception : >" + e.toString());
	 	    	System.out.println("Exception : > " + e.getMessage().toString()); 
	 	    }
	 			    
	 		return result; 
	     }
	     
	     public DeskUser changePassword(DeskUserLoginResetRequest p_loginResetObj) {
	 		 	DeskUser result = new DeskUser(); 	    		

	 		 	// gggg.fn_change_password(
	 			//	p_username in character varying,
	 			//	p_password in character varying,
	 			//	p_createdBy in character varying
	 		 	//)
	 		 				    
			    try {	    			    	
			    	SqlParameterSource params = new MapSqlParameterSource()
			    			.addValue("p_username", p_loginResetObj.getDskusername())
			    			.addValue("p_password", p_loginResetObj.getDskpassword())
			    			.addValue("p_createdBy", p_loginResetObj.getDskCreatedBy()); 
			    			    			
			    	String strQuery = "select gggg.fn_change_password("
							+ ":p_username,"
							+ ":p_password,"
							+ ":p_createdBy)";   

			    	Integer userId = dx.queryForObject(strQuery, params, int.class);	
			    	if(userId == null) {
			    		result = null; 
			    	}
			    	else {
			    		result = getUserById(userId); 
			    	}
			    }
			    catch(Exception e) {
			    	e.printStackTrace();
			    	System.out.println("Exception : > " + e.getMessage().toString()); 
			    }
					    
				return result; 
	    	
	     }
	 	 
		 
}
		 
		 