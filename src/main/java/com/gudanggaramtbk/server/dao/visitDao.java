package com.gudanggaramtbk.server.dao;

import java.util.List;

import com.gudanggaramtbk.server.model.Visit;
import com.gudanggaramtbk.server.model.request.VisitAssignAccessCardRequest;
import com.gudanggaramtbk.server.model.request.VisitEndRequest;
import com.gudanggaramtbk.server.model.request.VisitSaveRequest;

public interface visitDao {
	
	 public Visit getVisitbyId(Integer p_visitId); 
	 public List<Visit> getVisitbyguestId(Integer p_guestId); 
	 public List<Visit> getVisitExpired(String p_siteCode);
	 public Visit getVisitbySessionCode(String p_visitSessionCode);
	 public Visit getActiveVisitbyguestCardId(String p_guestCardId); 		 	 
	 public List<Visit> getVisitByDate(String p_siteCode, String p_visitDate); 
	 public Visit setSaveVisit(VisitSaveRequest p_obj_visitReq); 
	 public Visit setendVisit(VisitEndRequest p_endVisitObj); 
	 public Visit setAssignAccessCard(VisitAssignAccessCardRequest p_VisitAssignCardObj);
		 
}
