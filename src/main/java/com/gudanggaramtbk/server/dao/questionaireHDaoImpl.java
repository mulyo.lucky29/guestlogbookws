package com.gudanggaramtbk.server.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.gudanggaramtbk.server.mapper.questionaireHMapper;
import com.gudanggaramtbk.server.model.QuestionaireH;
import com.gudanggaramtbk.server.model.request.QuestionaireHSaveRequest;


@Repository
public class questionaireHDaoImpl implements questionaireHDao{
	 @Autowired 
	 NamedParameterJdbcTemplate dx; 

	 @Autowired
	    JdbcTemplate ds;

	 String queryBase = "select qh.quehid,"
	 		+ "qh.quehname,"
	 		+ "qh.quehdesc "
	 		+ "from gggg.m_queh qh "
	 		+ "where 1 = 1 "; 

	 public QuestionaireH getQuestionaireById(Integer p_queId){
			QuestionaireH result; 
					  
			 final String strQuery = queryBase 
					    + " and qh.quehid = ? ";    

			 try {		
				  result = (QuestionaireH) ds.queryForObject(strQuery, new Object[]{p_queId}, new questionaireHMapper());						
				  return result; 
			  }
			  catch(Exception e) {
			    	e.printStackTrace();
			    	System.out.println("Exception : > " + e.getMessage().toString()); 
			    	return null; 
			  }		  
			 
	} 
	 
	 public List<QuestionaireH> getListQuestionaire(){
			List<QuestionaireH> result; 
					  
			 final String strQuery = queryBase 
				 		+ " order by qh.quehid ";
			 
			 try {		
				  result = ds.query(strQuery, new questionaireHMapper());			
				  if(result.isEmpty()) {				  
				  }
				  return result; 
			  }
			  catch(Exception e) {
			    	e.printStackTrace();
			    	System.out.println("Exception : > " + e.getMessage().toString()); 
			    	return null; 
			  }		  
	} 

 	 public QuestionaireH saveQuehForm(QuestionaireHSaveRequest p_queHSave) {	
 		QuestionaireH result = new QuestionaireH(); 	    		

		 	// gggg.fn_save_queh(
 			//		p_quehId in bigint,
 			//		p_quehName in character varying,
 			//		p_quehDesc in character varying,
 			//		p_createdBy in character varying
		 	// )
		 				    
		    try {	    			    	
		    	SqlParameterSource params = new MapSqlParameterSource()
		    			.addValue("p_quehId", p_queHSave.getQuehid())
		    			.addValue("p_quehName", p_queHSave.getQuehname())
		    			.addValue("p_quehDesc", p_queHSave.getQuehdesc())			    			
		    			.addValue("p_createdBy", p_queHSave.getCreatedBy()); 
		    			    			
		    	String strQuery = "select gggg.fn_save_queh("
						+ ":p_quehId,"
						+ ":p_quehName,"
						+ ":p_quehDesc,"
						+ ":p_createdBy)";   

		    	Integer formId = dx.queryForObject(strQuery, params, int.class);	
		    	if(formId == null) {
		    		result = null; 
		    	}
		    	else {
		    		result = getQuestionaireById(formId); 
		    	}
		    }
		    catch(Exception e) {
		    	e.printStackTrace();
		    	System.out.println("Exception : > " + e.getMessage().toString()); 
		    }
				    
			return result; 
		}	 

     public QuestionaireH deleteQuehForm(Integer p_queId) {
    	 QuestionaireH result = new QuestionaireH(); 	    		
     	    	 	
     	// query first before deleted 
     	result = getQuestionaireById(p_queId);    	    	
 	    try {	    			    	
 	    	SqlParameterSource params = new MapSqlParameterSource()
 	    			.addValue("p_quehId", p_queId);
 	    	
 	    	String strQuery = "select gggg.fn_delete_queh(:p_quehId);"; 
 					
 	    	Integer deletedFormId = dx.queryForObject(strQuery, params, int.class);	
 	    	if(deletedFormId == null) {
 	    		result = null; 
 	    	}
 	    }
 	    catch(Exception e) {
 	    	e.printStackTrace();
 	    	System.out.println("Exception : > " + e.getMessage().toString()); 
 	    }
 			    
 		return result; 
     }

 	 
}
