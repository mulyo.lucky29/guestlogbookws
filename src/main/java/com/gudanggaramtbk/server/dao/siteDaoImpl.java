package com.gudanggaramtbk.server.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.gudanggaramtbk.server.mapper.siteMapper;
import com.gudanggaramtbk.server.model.DeskUser;
import com.gudanggaramtbk.server.model.Site;
import com.gudanggaramtbk.server.model.request.DeskUserLoginSaveRequest;
import com.gudanggaramtbk.server.model.request.SiteSaveRequest;
 
@Repository
public class siteDaoImpl implements siteDao {
	 @Autowired 
	 NamedParameterJdbcTemplate dx; 
	 
	 @Autowired
	    JdbcTemplate ds;
		 
	 
		 String queryBase = "select ms.siteId,"
		 		+ "       ms.siteCode,"
		 		+ "       ms.siteName,"
		 		+ "       ms.isActive, "
		 		+ "       ms.formaccessmentid " 
		 		+ "from gggg.m_site ms "
		 		+ "where 1 = 1 "; 
 
	 
		 public Site getSitebyId(Integer p_siteId){
				Site result; 
						  
				 final String strQuery = queryBase 
						    + " and ms.siteId = ? "    
					 		+ " order by ms.siteCode ";
				 
				 try {		
					  result = (Site) ds.queryForObject(strQuery, new Object[]{p_siteId}, new siteMapper());						
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
			 } 

		 public Site getSitebyCode(String p_siteCode){
				Site result; 
						  
				 final String strQuery = queryBase 
						    + " and ms.siteCode = ? "    
					 		+ " order by ms.siteCode ";
				 
				 try {		
					  result = (Site) ds.queryForObject(strQuery, new Object[]{p_siteCode}, new siteMapper());						
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
			 } 
		 
		 public List<Site> getListSite(){
				List<Site> result; 
						  
				 final String strQuery = queryBase 
					 		+ " order by ms.siteCode ";
				 
				 try {		
					  result = ds.query(strQuery, new siteMapper());			
					  if(result.isEmpty()) {				  
					  }
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
		} 

	 	 public Site saveSite(SiteSaveRequest p_siteSaveObj) {	
	 		 	Site result = new Site(); 	    		

	 		 	// gggg.fn_save_site(
	 			//		p_siteCode in character varying,
	 			//		p_siteName in character varying,
	 			//		p_formId in bigint,
	 			//		p_isActive in character varying,
	 			//		p_createdBy in character varying
	 		 	// )
	 		 				    
			    try {	    			    	
			    	SqlParameterSource params = new MapSqlParameterSource()
			    			.addValue("p_siteCode", p_siteSaveObj.getSiteCode())
			    			.addValue("p_siteName", p_siteSaveObj.getSiteName())
			    			.addValue("p_formId", p_siteSaveObj.getFormaccessmentid())			    			
			    			.addValue("p_isactive", p_siteSaveObj.getIsActive())
			    			.addValue("p_createdBy", p_siteSaveObj.getCreatedBy()); 
			    			    			
			    	String strQuery = "select gggg.fn_save_site("
							+ ":p_siteCode,"
							+ ":p_siteName,"
							+ ":p_formId,"
							+ ":p_isactive,"
							+ ":p_createdBy)";   

			    	Integer siteId = dx.queryForObject(strQuery, params, int.class);	
			    	if(siteId == null) {
			    		result = null; 
			    	}
			    	else {
			    		result = getSitebyId(siteId); 
			    	}
			    }
			    catch(Exception e) {
			    	e.printStackTrace();
			    	System.out.println("Exception : > " + e.getMessage().toString()); 
			    }
					    
				return result; 
			}
			 
}
