package com.gudanggaramtbk.server.dao;

import java.util.List;
import com.gudanggaramtbk.server.model.Site;
import com.gudanggaramtbk.server.model.request.SiteSaveRequest;

public interface siteDao {

	 public Site getSitebyId(Integer p_siteId); 
	 public Site getSitebyCode(String p_siteCode); 
 	 public Site saveSite(SiteSaveRequest p_siteSaveObj); 
	 public List<Site> getListSite(); 


}
