package com.gudanggaramtbk.server.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.gudanggaramtbk.server.mapper.lookupMapper;
import com.gudanggaramtbk.server.model.Lookup;

@Repository
public class lookupDaoImpl implements lookupDao{
	 @Autowired
	    JdbcTemplate ds;
		 
		 String queryBase = "select lo.lookupId,"
		 		+ " lo.lookupGroupCode,"
		 		+ "	lo.lookupValue,"
		 		+ "	lo.lookupDescription,"
		 		+ "	lo.lookupOrderNo,"
		 		+ " lo.isActive "
		 		+ "from gggg.m_lookup lo  "
		 		+ "where 1 = 1 "; 
		 
		 public Lookup getlookupyId(Integer p_lookupId){
				Lookup result; 
						  
				 final String strQuery = queryBase 
						    + " and lo.lookupId = ? "    
					 		+ " order by lo.lookupGroupCode, coalesce(lo.lookupOrderNo,0)  ";
				 
				 try {		
					  result = (Lookup) ds.queryForObject(strQuery, new Object[]{p_lookupId}, new lookupMapper());						
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
			 } 

		 public List<Lookup> getListLookupByGroup(String p_group){
				List<Lookup> result; 
						  
				 final String strQuery = queryBase 
						    + " and lo.lookupGroupCode = ? "    
					 		+ " order by lo.lookupGroupCode, coalesce(lo.lookupOrderNo,0) ";
				 
				 try {		
					  result = ds.query(strQuery,  new Object[]{p_group}, new lookupMapper());			
					  if(result.isEmpty()) {				  
					  }
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
		} 
		 
}