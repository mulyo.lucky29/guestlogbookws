package com.gudanggaramtbk.server.dao;

import java.util.List;
import com.gudanggaramtbk.server.model.Contact;
import com.gudanggaramtbk.server.model.Guest;
import com.gudanggaramtbk.server.model.request.ContactSaveRequest;

public interface contactDao {

 	 public Contact getContact(Integer p_contactId); 
	 public List<Contact> getListContactBySite(String p_siteCode); 
	 public Contact saveContact(ContactSaveRequest p_obj_contact); 
	 public Contact deleteContact(Integer p_contactId); 		 
}
