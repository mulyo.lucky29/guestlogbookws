package com.gudanggaramtbk.server.dao;

import java.util.List;
import com.gudanggaramtbk.server.model.Guest;

public interface guestDao {

	 public Guest getGuestById(Integer p_guestId); 
	 public Guest getGuestCardId(String p_guestCardId); 
	 public List<Guest> getListGuest(); 
	 public Guest saveGuestInfo(Guest p_obj_guest); 
}
