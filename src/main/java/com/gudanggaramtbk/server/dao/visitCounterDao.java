package com.gudanggaramtbk.server.dao;

import com.gudanggaramtbk.server.model.VisitCounter;

public interface visitCounterDao {
	 public VisitCounter getVisitCounter(Integer p_siteId, String p_date); 

}
