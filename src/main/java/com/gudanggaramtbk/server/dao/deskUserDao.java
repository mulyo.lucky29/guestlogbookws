package com.gudanggaramtbk.server.dao;

import java.util.List;

import com.gudanggaramtbk.server.model.Contact;
import com.gudanggaramtbk.server.model.DeskUser;
import com.gudanggaramtbk.server.model.request.DeskUserLoginSaveRequest;
import com.gudanggaramtbk.server.model.request.DeskUserLoginRequest;
import com.gudanggaramtbk.server.model.request.DeskUserLoginResetRequest;

public interface deskUserDao {

	 public DeskUser getUserById(Integer p_userId); 
	 public DeskUser getLogin(DeskUserLoginRequest p_loginReqObj); 
 	 public DeskUser saveLogin(DeskUserLoginSaveRequest p_loginAccObj); 
 	 public DeskUser changePassword(DeskUserLoginResetRequest p_loginResetObj);     	 	 
 	 public List<DeskUser> getListUserBySite(Integer p_siteId); 
	 public List<DeskUser> getListAllUser(); 
	 public DeskUser deleteLogin(Integer p_userId); 
	 
	 
	 
}


