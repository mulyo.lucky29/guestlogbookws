package com.gudanggaramtbk.server.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import com.gudanggaramtbk.server.mapper.visitCounterMapper;
import com.gudanggaramtbk.server.model.VisitCounter;

@Repository
public class visitCounterDaoImpl implements visitCounterDao {
	 @Autowired 
	 NamedParameterJdbcTemplate dx; 
	 
	 @Autowired
	    JdbcTemplate ds;
		 
	 String queryBase = "select sc_siteid, "
			 		+ "sc_sitecode, "
			 		+ "sc_sitename, "
			 		+ "sc_visitdate, "
			 		+ "sc_person_visit, "
			 		+ "sc_person_active_visit, "
			 		+ "sc_person_ended_normal_visit, "
			 		+ "sc_person_ended_cancelled_visit, "
			 		+ "sc_person_visit_expired,"
			 		+ "sc_session_visit, "
			 		+ "sc_session_active_visit, "
			 		+ "sc_session_ended_normal_visit, "
			 		+ "sc_session_ended_cancelled_visit, "
			 		+ "sc_session_visit_expired ";
	 	 
	 
	 public VisitCounter getVisitCounter(Integer p_siteId, String p_date) {
			VisitCounter result; 
			Date v_visitDate; 
								  
			 final String strQuery = queryBase 
					 	+ " from gggg.fn_select_counter_visit(?, ?) sc "
				 		+ " where 1 = 1 "; 
			 
			 System.out.println(strQuery); 
			 
			 try {
				 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				 v_visitDate = formatter.parse(p_date);

				 try {		
					  result = (VisitCounter) ds.queryForObject(strQuery, new Object[]{p_siteId, v_visitDate}, new visitCounterMapper());	
					  return result; 
				  }
				 catch (EmptyResultDataAccessException e) {
						return null;
				  }
			 }
			 catch (ParseException e) {
			        e.printStackTrace();
			    	System.out.println("Exception : > " + e.getMessage().toString()); 
			    	return null; 
			 }  
		 } 
	 
}
