package com.gudanggaramtbk.server.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.gudanggaramtbk.server.mapper.lookupMapper;
import com.gudanggaramtbk.server.mapper.questionaireHMapper;
import com.gudanggaramtbk.server.mapper.questionaireMapper;
import com.gudanggaramtbk.server.model.Questionaire;
import com.gudanggaramtbk.server.model.QuestionaireH;
import com.gudanggaramtbk.server.model.Site;
import com.gudanggaramtbk.server.model.request.QuestionaireHSaveRequest;
import com.gudanggaramtbk.server.model.request.QuestionaireSaveRequest;
import com.gudanggaramtbk.server.model.request.SiteSaveRequest;

@Repository
public class questionaireDaoImpl implements questionaireDao{
	 @Autowired 
	 NamedParameterJdbcTemplate dx; 
	 
	 @Autowired
	 JdbcTemplate ds;

	 String queryBase = "select ql.quehid,"
	 		+ "	   ql.quedid,"
	 		+ "	   ql.quelineno,"
	 		+ "	   ql.quetext,"
	 		+ "	   qh.quehname,"
	 		+ "	   qh.quehdesc "
	 		+ "from gggg.m_queh qh,"
	 		+ "     gggg.m_quel ql "
	 		+ "where qh.quehid = ql.quehid "; 

	 public Questionaire getQuestionPointById(Integer p_quelId){
		 Questionaire result; 
						  
			final String strQuery = queryBase 
				 + " and ql.quedid = ? ";    
	
			try {		
					result = (Questionaire) ds.queryForObject(strQuery, new Object[]{p_quelId}, new questionaireMapper());						
					return result; 
			}
			catch(Exception e) {
			   	e.printStackTrace();
			  	System.out.println("Exception : > " + e.getMessage().toString()); 
			   	return null; 
			}		  				 
	 } 
	 	 
	 public List<Questionaire> getListQuestionaireById(Integer p_queId){
			List<Questionaire> result; 
					  
			 final String strQuery = queryBase 
					    + " and ql.quehid = ? "    
				 		+ " order by coalesce(ql.quelineno,0) ";
			 
			 try {		
				  result = ds.query(strQuery,  new Object[]{p_queId}, new questionaireMapper());			
				  if(result.isEmpty()) {				  
				  }
				  return result; 
			  }
			  catch(Exception e) {
			    	e.printStackTrace();
			    	System.out.println("Exception : > " + e.getMessage().toString()); 
			    	return null; 
			  }		  
	}
	  		 
	 public Questionaire saveQuestionPoint(QuestionaireSaveRequest p_queSave) {	
 		Questionaire result = new Questionaire(); 	    		

 		  		//	gggg.fn_save_quel(
 				//		p_quelId in bigint, 
 				//		p_quehId in bigint,
 				//		p_quelineno in bigint, 
 				//		p_quetext in character varying,
 				//		p_createdBy in character varying
 				// )
		 				    
		    try {	    			    	
		    	SqlParameterSource params = new MapSqlParameterSource()
		    			.addValue("p_quelId", p_queSave.getQuedid())
		    			.addValue("p_quehId", p_queSave.getQuehid())
		    			.addValue("p_quelineno", p_queSave.getQuelineno())	
		    			.addValue("p_quetext", p_queSave.getQuetext())			    			
		    			.addValue("p_createdBy", p_queSave.getCreatedBy()); 
		    			    			
		    	String strQuery = "select gggg.fn_save_quel("
						+ ":p_quelId,"
		    			+ ":p_quehId,"
						+ ":p_quelineno,"
						+ ":p_quetext,"
						+ ":p_createdBy)";   

		    	Integer quePointId = dx.queryForObject(strQuery, params, int.class);	
		    	if(quePointId == null) {
		    		result = null; 
		    	}
		    	else {
		    		result = getQuestionPointById(quePointId); 
		    	}
		    }
		    catch(Exception e) {
		    	e.printStackTrace();
		    	System.out.println("Exception : > " + e.getMessage().toString()); 
		    }
				    
			return result; 
		}	 

	 
     public Questionaire deleteQuestionPointById(Integer p_quelId) {
    	 Questionaire result = new Questionaire(); 	    		
     	    	 	
     	// query first before deleted 
     	result = getQuestionPointById(p_quelId);    	    	
 	    try {	    			    	
 	    	SqlParameterSource params = new MapSqlParameterSource()
 	    			.addValue("p_quelId", p_quelId);
 	    	
 	    	String strQuery = "select gggg.fn_delete_quel(:p_quelId);"; 
 					
 	    	Integer deletedFormId = dx.queryForObject(strQuery, params, int.class);	
 	    	if(deletedFormId == null) {
 	    		result = null; 
 	    	}
 	    }
 	    catch(Exception e) {
 	    	e.printStackTrace();
 	    	System.out.println("Exception : > " + e.getMessage().toString()); 
 	    }
 			    
 		return result; 
     }

	 
}