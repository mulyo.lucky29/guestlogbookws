package com.gudanggaramtbk.server.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.gudanggaramtbk.server.mapper.visitMapper;
import com.gudanggaramtbk.server.model.request.VisitAssignAccessCardRequest;
import com.gudanggaramtbk.server.model.request.VisitEndRequest;
import com.gudanggaramtbk.server.model.request.VisitSaveRequest;
import com.gudanggaramtbk.server.model.Visit;

@Repository
public class visitDaoImpl implements visitDao {
	 @Autowired 
	 NamedParameterJdbcTemplate dx; 
	 
	 @Autowired
	    JdbcTemplate ds;
		 
		 String queryBase = "select vs.visitId,"
		 		+ "	   vs.visitSessionCode,"
		 		+ "	   vs.visitDate,"
		 		+ "	   vs.visitPurpose,"
		 		+ "	   vs.guestId,"
		 		+ "	   vs.guestCardId,"
		 		+ "	   vs.guestName,"
		 		+ "	   vs.guestPhone,"
		 		+ "	   vs.guestEmail,"
		 		+ "	   vs.guestCompany,"
		 		+ "	   vs.guestPictureUrl,"
		 		+ "	   vs.guestPictureCardId,"
		 		+ "	   vs.siteId,"
		 		+ "	   vs.siteCode,"
		 		+ "	   vs.siteName,"
		 		+ "    vs.sitespecific,"
		 		+ "	   vs.contactId,"
		 		+ "    mc.tenantcompany," 
		 		+ "	   mc.contacttype, "		 		
		 		+ "	   vs.contactName,"
		 		+ "	   vs.contactEmail,"
		 		+ "    mc.contactphone,"		 		
		 		+ "	   vs.contactExt,"
		 		+ "	   vs.contactAltName,"
		 		+ "	   vs.checkIn,"
		 		+ "	   vs.checkOut,"
		 		+ "    vs.creation_date startVisitDate,"
		 		+ "    vs.endVisitDate,"   
		 		+ "	   vs.endVisitFlag,"
		 		+ "	   vs.endVisitReason,"
		 		+ "    vs.endVisitBy,"
		 		+ "    vs.assuranceCardType,"
		 		+ "    vs.assuranceCardNo,"
		 		+ "    vs.accessCardTag,"
		 		+ "    vs.accessCardPairNo, "
		 		+ "    vs.formaccessment "    
		 		+ "from gggg.m_visit vs left join gggg.m_contact mc on (vs.contactId = mc.contactid) "
		 		+ "where 1 = 1 "; 
	 
		 public Visit getActiveVisitbyguestCardId(String p_guestCardId){
				Visit result; 
						  
				 final String strQuery = queryBase 
						    + " and vs.guestCardId = ? "    
						    + " and vs.endvisitflag is null "
					 		+ " order by vs.visitId "
						    + " limit 1";
				 
				 try {		
					  result = (Visit) ds.queryForObject(strQuery, new Object[]{p_guestCardId}, new visitMapper());						
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
			 } 

		 public List<Visit> getVisitbyguestId(Integer p_guestId){
				List<Visit> result; 
						  
				 final String strQuery = queryBase 
						    + " and vs.guestId = ? "    
					 		+ " order by vs.visitId";
				 
				 try {		
					  result = ds.query(strQuery, new Object[]{p_guestId}, new visitMapper());			
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
		 } 

		 public List<Visit> getVisitExpired(String p_siteCode){
				List<Visit> result; 
						  
				 final String strQuery = queryBase 
						    + " and vs.siteCode = ? "
						    + " and vs.endvisitdate is null "
						    + "	and vs.visitdate < ((now() - interval '1 DAY') ::DATE) "    
					 		+ " order by vs.visitId";
				 
				 try {		
					  result = ds.query(strQuery, new Object[]{p_siteCode}, new visitMapper());			
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
		 } 

		 
		 public List<Visit> getVisitByDate(String p_siteCode, String p_visitDate) {
			 List<Visit> result; 			 
			 Date v_visitDate; 

			 final String strQuery = queryBase 
					 	+ " and vs.siteCode = ? "
					 	+ " and vs.visitDate = ? "
				 		+ " order by vs.visitId ";
			 
			 try {
				 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				 v_visitDate = formatter.parse(p_visitDate);

				 try {		
					  result = ds.query(strQuery, new Object[]{p_siteCode, v_visitDate}, new visitMapper());			
					  if(result.isEmpty()) {				  
					  }
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }						 
		     } 
			 catch (ParseException e) {
		        e.printStackTrace();
		    	System.out.println("Exception : > " + e.getMessage().toString()); 
		    	return null; 
			 }			  			 
		 }
		 
		 public Visit getVisitbyId(Integer p_visitId){
				Visit result; 
						  
				 final String strQuery = queryBase 
						    + " and vs.visitId = ? "    
					 		+ " order by vs.visitId ";
				 
				 try {		
					  result = (Visit) ds.queryForObject(strQuery, new Object[]{p_visitId}, new visitMapper());						
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
			 } 

		 public Visit getVisitbySessionCode(String p_visitSessionCode){
				Visit result; 
						  
				 final String strQuery = queryBase 
						    + " and vs.visitSessionCode = ? "    
					 		+ " order by vs.visitId ";
				 
				 try {		
					  result = (Visit) ds.queryForObject(strQuery, new Object[]{p_visitSessionCode}, new visitMapper());						
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
			 }

	 	 public Visit setSaveVisit(VisitSaveRequest p_obj_visitReq) {		 		 
			    Visit result = new Visit(); 	    		
			    	
			    //	gggg.fn_save_visit(
				//		 p_siteid in bigint,	 
				// 		 p_guestid in bigint,	 
				// 		 p_contactid in bigint,
				// 		 p_visitpurpose in character varying,
				// 		 p_contactaltname in character varying
				// 		 p_assuranceCardType in character varying,
				// 		 p_assuranceCardNo in character varying,
			    //		 p_formaccessment in character varying
			    // );
			 
			    
			    try {				    		    				     
			    	SqlParameterSource params = new MapSqlParameterSource()
			    			.addValue("p_siteid", p_obj_visitReq.getSiteid())
			    			.addValue("p_guestid", p_obj_visitReq.getGuestid())
			    			.addValue("p_contactid", p_obj_visitReq.getContactid())			    			
			    			.addValue("p_visitpurpose", p_obj_visitReq.getVisitpurpose())
			    			.addValue("p_contactaltname", p_obj_visitReq.getContactaltname())
			    			.addValue("p_assuranceCardType", p_obj_visitReq.getAssuranceCardType())
			    			.addValue("p_assuranceCardNo", p_obj_visitReq.getAssuranceCardNo())
			    			.addValue("p_formaccessment", p_obj_visitReq.getFormaccessment());
			    						    			
			    	String strQuery = "select gggg.fn_save_visit("
							+ ":p_siteid,"
							+ ":p_guestid,"
							+ ":p_contactid,"
							+ ":p_visitpurpose,"
							+ ":p_contactaltname,"
							+ ":p_assuranceCardType,"
							+ ":p_assuranceCardNo," 
							+ ":p_formaccessment)"; 

			    	Integer visitId = dx.queryForObject(strQuery, params, int.class);	
			    	 
			    	if(visitId == null) {
			    		result = null; 
			    	}
			    	else {
			    		result = getVisitbyId(visitId); 
			    	}
			    }
			    catch(Exception e) {
			    	e.printStackTrace();
			    	//log.debug("Exception : >" + e.toString());
			    	System.out.println("Exception : > " + e.getMessage().toString()); 
			    }
					    
				return result;
			}

	 	 public Visit setendVisit(VisitEndRequest p_endVisitObj) {			 		 
			    Visit result = new Visit(); 	    		
			    	
			    //	gggg.fn_end_visit(
				// 		p_endvisitid in bigint,	 
				// 		p_endvisitreason in character varying,
				// 		p_endvisitby in character varying 
			 	//)
			 			    
			    try {
			    	// cache first 
			    	result = getVisitbyId(p_endVisitObj.getEndVisitId()); 
			    			
			    	SqlParameterSource params = new MapSqlParameterSource()
			    			.addValue("p_endvisitid", p_endVisitObj.getEndVisitId())
			    			.addValue("p_endvisitreason", p_endVisitObj.getEndVisitreason())
			    			.addValue("p_endvisitby", p_endVisitObj.getEndVisitby());			    			
			    						    			
			    	String strQuery = "select gggg.fn_end_visit("
							+ ":p_endvisitid,"
							+ ":p_endvisitreason,"
							+ ":p_endvisitby)";
			    	
			    	Integer visitId = dx.queryForObject(strQuery, params, int.class);	
			    	 
			    	if(visitId < 0) {
			    		// if visit session already dismiss then output result -1 
			    		result.setVisitId(visitId); 
			    	}
			    	else {
			    		result = getVisitbyId(visitId); 
			    	}
			    }
			    catch(Exception e) {
			    	e.printStackTrace();
			    	//log.debug("Exception : >" + e.toString());
			    	System.out.println("Exception : > " + e.getMessage().toString()); 
			    }					    
				return result;
			}

	 	 public Visit setAssignAccessCard(VisitAssignAccessCardRequest p_VisitAssignCardObj) {		 		 
			    Visit result = new Visit(); 	    		
			    	
			    
			    //gggg.fn_save_assign_accesscard(
			    //		p_visitId in bigint, 
			    //		p_accesscardtag in character varying,
			    //		p_accesscardpairno in character varying,
 			    // 		p_user in character varying
			    		
			 			    
			    try {				    		    				     
			    	SqlParameterSource params = new MapSqlParameterSource()
			    			.addValue("p_visitId", p_VisitAssignCardObj.getVisitId())
			    			.addValue("p_accesscardtag", p_VisitAssignCardObj.getAccessCardTag())
			    			.addValue("p_accesscardpairno", p_VisitAssignCardObj.getAccessCardPairNo())
			    			.addValue("p_user", p_VisitAssignCardObj.getUsername());			    			
			    						    			

			    	String strQuery = "select gggg.fn_save_assign_accesscard("
							+ ":p_visitId,"
							+ ":p_accesscardtag,"
							+ ":p_accesscardpairno,"							
							+ ":p_user)";
			    	
			    	Integer visitId = dx.queryForObject(strQuery, params, int.class);	
			    	 
			    	if(visitId == null) {
			    		result = null; 
			    	}
			    	else {
			    		result = getVisitbyId(visitId); 
			    	}
			    }
			    catch(Exception e) {
			    	e.printStackTrace();
			    	System.out.println("Exception : > " + e.getMessage().toString()); 
			    }
					    
				return result;
			}
}