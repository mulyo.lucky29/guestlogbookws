package com.gudanggaramtbk.server.dao;

import java.util.List;
import com.gudanggaramtbk.server.model.CheckLog;
import com.gudanggaramtbk.server.model.request.VisitCheckLogRequest;

public interface checkLogDao {
	 public CheckLog getCheckLogById(Integer p_check_id); 
	 public List<CheckLog> getListCheckLogByVisitCode(String p_visit_code); 
	 public List<CheckLog> getListCheckLogByVisitId(Integer p_visit_id); 
 	 public CheckLog setDoCheckLog(VisitCheckLogRequest p_obj_checkLogReq);	 		 

}
