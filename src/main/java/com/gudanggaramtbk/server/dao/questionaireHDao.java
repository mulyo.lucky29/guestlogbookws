package com.gudanggaramtbk.server.dao;

import java.util.List;

import com.gudanggaramtbk.server.model.QuestionaireH;
import com.gudanggaramtbk.server.model.request.QuestionaireHSaveRequest;

public interface questionaireHDao {

	 public QuestionaireH saveQuehForm(QuestionaireHSaveRequest p_queHSave);
     public QuestionaireH deleteQuehForm(Integer p_queId); 
	 public QuestionaireH getQuestionaireById(Integer p_queId); 
 	 public List<QuestionaireH> getListQuestionaire();

}
