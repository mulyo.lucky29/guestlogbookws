package com.gudanggaramtbk.server.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.gudanggaramtbk.server.mapper.checkLogMapper;
import com.gudanggaramtbk.server.model.CheckLog;
import com.gudanggaramtbk.server.model.request.VisitCheckLogRequest;

@Repository
public class checkLogDaoImpl implements checkLogDao {
	 @Autowired 
	 NamedParameterJdbcTemplate dx; 
	 
	 @Autowired
	    JdbcTemplate ds;
		 
		 String queryBase = "select cl.checklogid,"
		 		+ "	   cl.visitid,"
		 		+ "	   cl.visitsessioncode,"
		 		+ "	   cl.checkedtime,"
		 		+ "	   cl.checktyped,"
		 		+ "	   cl.checkedterminalid "
		 		+ "from gggg.m_checklog cl "
		 		+ "where 1 = 1 "; 

		 public CheckLog getCheckLogById(Integer p_check_id){
			 CheckLog result; 
						  
				 final String strQuery = queryBase 
					 		+ " and cl.checklogid = ? "
					 		+ " order by cl.checklogid ";		
								 
				 try {		
					  result = (CheckLog) ds.queryForObject(strQuery, new Object[]{p_check_id}, new checkLogMapper());						
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
		} 
		 
		 public List<CheckLog> getListCheckLogByVisitCode(String p_visit_code){
				List<CheckLog> result; 
						  
				 final String strQuery = queryBase 
						    + " and cl.visitsessioncode = ? "   
					 		+ " order by cl.visitid desc";
				 
				 try {		
					  result = ds.query(strQuery,  new Object[] {p_visit_code}, new checkLogMapper());			

					  if(result.isEmpty()) {				  
					  }
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
			} 		 

		 public List<CheckLog> getListCheckLogByVisitId(Integer p_visit_id){
				List<CheckLog> result; 
						  
				 final String strQuery = queryBase 
						    + " and cl.visitid = ? "   
					 		+ " order by cl.visitid desc";
				 
				 try {		
					  result = ds.query(strQuery,  new Object[] {p_visit_id}, new checkLogMapper());			

					  if(result.isEmpty()) {				  
					  }
					  return result; 
				  }
				  catch(Exception e) {
				    	e.printStackTrace();
				    	System.out.println("Exception : > " + e.getMessage().toString()); 
				    	return null; 
				  }		  
			} 			 

	 	 public CheckLog setDoCheckLog(VisitCheckLogRequest p_obj_checkLogReq) {		 		 
	 		 	CheckLog result = new CheckLog(); 	    		
			    	
			    //	gggg.fn_do_checkinout( 	 
	 			//		 p_session_code character varying, 
	 			// 		 p_terminal_id  character varying
			    // );
			 
			    try {				    		    				     
			    	SqlParameterSource params = new MapSqlParameterSource()
			    			.addValue("p_session_code", p_obj_checkLogReq.getVisitSessionCode())
			    			.addValue("p_terminal_id", p_obj_checkLogReq.getTerminalId()); 
			    						    			

			    	String strQuery = "select gggg.fn_do_checkinout("
							+ ":p_session_code,"
							+ ":p_terminal_id)"; 

			    	Integer checkLogId = dx.queryForObject(strQuery, params, int.class);	
			    	 
			    	if(checkLogId < 0) {
			    		result.setCheckLogId(checkLogId);
			    	}
			    	else {
			    		result = getCheckLogById(checkLogId); 
			    	}
			    }
			    catch(Exception e) {
			    	e.printStackTrace();
			    	System.out.println("Exception : > " + e.getMessage().toString()); 
			    }					    
				return result;
			}

}
