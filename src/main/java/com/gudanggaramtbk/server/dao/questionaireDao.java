package com.gudanggaramtbk.server.dao;

import java.util.List;
import com.gudanggaramtbk.server.model.Questionaire;
import com.gudanggaramtbk.server.model.request.QuestionaireSaveRequest;

public interface questionaireDao {
	
	 public List<Questionaire> getListQuestionaireById(Integer p_queId);
 	 public Questionaire saveQuestionPoint(QuestionaireSaveRequest p_queSave); 
     public Questionaire deleteQuestionPointById(Integer p_quelId); 

     
}
