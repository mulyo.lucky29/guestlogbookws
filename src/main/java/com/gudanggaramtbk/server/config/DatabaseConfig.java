package com.gudanggaramtbk.server.config;

import java.lang.reflect.InvocationTargetException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;

@Configuration
public class DatabaseConfig {
	  @Autowired
	    private Environment env;
	        
		@Bean 
		@Primary
		@ConfigurationProperties(prefix="ms.datasource")
		public DataSource msDataSource() throws IllegalAccessException, InvocationTargetException, InstantiationException {
	    	String datasource_driver_class_name= env.getProperty("pg.datasource.driver-class-name");
	    	String datasource_url= env.getProperty("pg.datasource.url"); 
	    	String datasource_username = env.getProperty("pg.datasource.username");
	    	String datasource_password= env.getProperty("pg.datasource.password");

		    final DataSource dataSource = DataSourceBuilder
		    		.create()
		    		.driverClassName(datasource_driver_class_name)
		    		.url(datasource_url)
		     		.username(datasource_username)
		     		.password(datasource_password)
		     		.build();

		    return dataSource; 
		}
		
}