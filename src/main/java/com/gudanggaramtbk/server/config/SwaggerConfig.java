package com.gudanggaramtbk.server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityScheme;

@Configuration
public class SwaggerConfig {

	// for swagger oas3
		@Bean
		    public OpenAPI customOpenAPI(@Value("${spring.application.description}") String appDesciption,
		    							 @Value("${spring.application.version}") String appVersion) {
			 	final String securitySchemeName = "bearerAuth";
			    //final String apiTitle = String.format("%s API", StringUtils.capitalize(moduleName));
			    		
			    return new OpenAPI()
			    	        .components(new Components().addSecuritySchemes(securitySchemeName,
			    	                    new SecurityScheme()
			    	                        .name(securitySchemeName)
			    	                        .type(SecurityScheme.Type.HTTP)
			    	                        .scheme("bearer")
			    	                        .bearerFormat("JWT")
			    	                )
			    	        )
			    	        // if security token will be applied globally 
				    		//.addSecurityItem(new SecurityRequirement().addList(securitySchemeName,Arrays.asList("read", "write")))	
			    	       		    
			    	        .info(new Info().title("API")
			    	        		.version(appVersion)
			    	        		.description(appDesciption)
			    	        		.termsOfService("http://swagger.io/terms/")
			    	        		.license(new License()
			    	        				.name("Apache 2.0")
			    	        				.url("http://springdoc.org")));

		 }
		
}