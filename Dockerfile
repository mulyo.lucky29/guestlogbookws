FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/ggguestlogbook-0.0.1-SNAPSHOT.jar
WORKDIR /opt/app
COPY ${JAR_FILE} ggguestlogbook-0.0.1-SNAPSHOT.jar
EXPOSE 8080
CMD ["java", "-jar", "ggguestlogbook-0.0.1-SNAPSHOT.jar"]